var elixir = require('laravel-elixir');
var gulp = require("gulp");
require('laravel-elixir-remove');

var Task = elixir.Task;

var path = {
    reactRoot: './resources/assets/react/**',
    reactMountForViews : './resources/assets/react/components/views/',
    reactComponentStyles : './resources/assets/react/styles/'
};

elixir.extend("watch", function(arguments) {
    new Task('react', function() {
        return gulp.start(['styles', 'scripts', 'sass', 'browserify', 'version']);
    }).watch(path.reactRoot);
});

elixir(function(mix) {

    mix.styles([
        'animate/animate.css',
        'font-awesome/font-awesome.css',
        'kendo/kendo.common.min.css',
        'kendo/kendo.default.min.css',
        'kendo/kendo.theme.flat.css',
        'bootstrap/bootstrap.css',
        'semantic/semantic.css',
        'app.css'
    ]);

    mix.styles('welcome.css', 'public/css/welcome.css');

    mix.scripts([
        'wow/wow.js',
        'jquery/jquery.min.js',
        'jquery/jquery.easing.1.3.js',
        'jquery/jquery.lazyload.min.js',
        'kendo/kendo.ui.core.min.js',
        'kendo/kendo.culture.zh-CHT.min.js',
        'bootstrap/bootstrap.js',
        'semantic/semantic.js',
        'icollect/control-store-table.js',
        'icollect/row-control-stack.js',
        'icollect/number-picker.js'
    ]);

    mix.browserify(
        'app.blade.js',
        'public/js/app.blade.js',
        path.reactMountForViews
    );

    mix.browserify(
        'admin/home.blade.js',
        'public/js/admin/home.blade.js',
        path.reactMountForViews
    );

    mix.sass(
        path.reactComponentStyles + '*.scss',
        'public/css/components.css'
    );

    mix.version(['public/css/*.css', 'public/css/*/*.css', 'public/js/*.js', 'public/js/*/*.js']);
    mix.copy('resources/assets/css/bootstrap/fonts', 'public/build/fonts');
    mix.copy('resources/assets/css/font-awesome/fonts', 'public/build/fonts');
    mix.copy('resources/assets/css/semantic/themes', 'public/build/css/themes');
});
