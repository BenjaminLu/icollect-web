<?php

namespace App\Jobs;

use App\Activity;
use App\Jobs\Job;
use App\QRCodeControl;
use App\Ticket;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class SendReminderEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $tickets;
    protected $user;
    protected $activity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tickets, User $user, Activity $activity)
    {
        $this->tickets = json_decode($tickets);
        $this->user = $user;
        $this->activity = $activity;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $urls = null;
        $tickets = $this->tickets;
        $user = $this->user;
        $activity = $this->activity;
        $domain = "http://" . config('app.qrcode_server_domain');
        $time = -microtime(true);
        foreach ($tickets as $ticket) {
            $ticketID = $ticket->id;
            $qrcodeContent = file_get_contents("http://" . config('app.tomcat_host') . ":" . config('app.tomcat_port') . "/icollect/sign/ticket/" . $ticketID);
            $qrcodeContent = iconv("ISO-8859-1", "UTF-8", $qrcodeContent);

            $token = $this->generateRandomString();
            $qrcode = new QRCodeControl;
            $qrcode->user_id = $user->id;
            $qrcode->token = $token;
            $qrcode->save();

            //save qrcode
            $filepath = storage_path("qrcode/" . $user->id . "/");
            if (!file_exists($filepath)) {
                mkdir($filepath, 0755, true);
            }
            QrCode::format('png')->size(300)->generate($qrcodeContent, $filepath . $qrcode->id . '.png');
            $urls[] = $domain . '/api/ticket/' . $qrcode->id . "/qrcode/token/" . $token;
        }
        $time += microtime(true);
        echo "Amount : " . count($tickets) . " Time(microseconds) : " . $time . PHP_EOL;

        $data = array(
            'userName' => $user->name,
            'ticketNumber' => count($tickets),
            'qrcode_urls' => $urls,
            'activity_image' => $domain . "/" . $activity->image,
            'activity_name' => $activity->name
        );

        Mail::queue('emails.buyTickets', $data, function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('iCollect購買票卷明細');
        });
    }

    private function generateRandomString($length = 30)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
