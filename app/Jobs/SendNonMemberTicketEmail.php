<?php

namespace App\Jobs;

use App\Activity;
use App\Jobs\Job;
use App\QRCodeControl;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class SendNonMemberTicketEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $tickets;
    protected $receiverEmail;
    protected $activity;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tickets, $receiverEmail, Activity $activity)
    {
        $this->tickets = json_decode($tickets);
        $this->receiverEmail = $receiverEmail;
        $this->activity = $activity;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $urls = null;
        $tickets = $this->tickets;
        $receiverEmail = $this->receiverEmail;
        $activity = $this->activity;
        $domain = "http://" . config('app.qrcode_server_domain');

        foreach ($tickets as $ticket) {
            $ticketID = $ticket->id;

            $qrcodeContent = file_get_contents("http://" . config('app.tomcat_host') . ":" . config('app.tomcat_port') . "/icollect/sign/ticket/" . $ticketID);
            $qrcodeContent = iconv("ISO-8859-1", "UTF-8", $qrcodeContent);

            $token = $this->generateRandomString();
            $qrcode = new QRCodeControl;
            $qrcode->user_id = 0;
            $qrcode->token = $token;
            $qrcode->save();

            //save qrcode
            $filepath = storage_path("qrcode/" . 0 . "/");
            if (!file_exists($filepath)) {
                mkdir($filepath, 0755, true);
            }
            QrCode::format('png')->size(300)->generate($qrcodeContent, $filepath . $qrcode->id . '.png');
            $urls[] = $domain . '/api/ticket/' . $qrcode->id . "/qrcode/token/" . $token;
        }

        $newTicketNumber = count($tickets);

        $data = array(
            'ticketNumber' => $newTicketNumber,
            'qrcodeUrls' => $urls,
            'activity_image' => $domain . "/" . $activity->image,
            'activity_name' => $activity->name
        );

        Mail::queue('emails.sendNonMemberBuyFromStoreTicket', $data, function ($m) use ($receiverEmail, $newTicketNumber) {
            $m->to($receiverEmail)->subject('您購買了' . $newTicketNumber . '張票卷');
        });

    }

    private function generateRandomString($length = 30)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
