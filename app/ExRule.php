<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ExRule extends Model {
    protected $fillable = ['name', 'body', 'quantity', 'image', 'activity_id'];
    public function activity()
    {
        return $this->belongsTo('App\Activity');
    }
}
