<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

    protected $fillable = ['name', 'email', 'password', 'address', 'tel', 'type'];
    protected $hidden = ['password'];

    //
    public function stores()
    {
        return $this->hasMany('App\Store');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }
}

