<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = ['name', 'body', 'location', 'gain_start', 'gain_end', 'gain_status'];

    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function gainRules()
    {
        return $this->hasMany('App\GainRule');
    }

    public function exRules()
    {
        return $this->hasMany('App\ExRule');
    }
}
