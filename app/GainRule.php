<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GainRule extends Model
{
    protected $fillable = ['body', 'value', 'activity_id'];

    //
    public function activity()
    {
        return $this->belongsTo('App\Activity');
    }
}
