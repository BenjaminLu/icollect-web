<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';

    //
    public function activity()
    {
        return $this->belongsTo('App\Activity');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
