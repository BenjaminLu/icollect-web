<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RSAKey extends Model
{
    protected $table = 'rsa_keys';
}
