<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotAdminAllowIP
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ips = config('app.admin_ip');
        $ipsArray = explode(',', $ips);
        $ip = $request->ip();
        if(in_array($ip, $ipsArray)) {
            return $next($request);
        }

        return abort(404);
    }
}
