<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class RedirectIfGroupNotLogin
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->groupCheck($request)) {

            if($request->ajax()) {
                return response()->json(array());
            }
            return redirect('/group/login');
        }
        return $next($request);
    }

    private function groupCheck($request)
    {
        return ($request->session()->get('group_check') == true);
    }

}
