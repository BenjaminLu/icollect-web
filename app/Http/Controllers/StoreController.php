<?php namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\NewStoreRequest;
use App\Http\Requests\UpdateStoreRequest;
use App\Store;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;
use Log;

class StoreController extends Controller
{
    public function showByGroup($gid)
    {
        if (session('group')->id != $gid) {
            abort(401);
        }

        $group = Group::find($gid);
        $stores = $group->stores()->orderBy('created_at', 'desc')->get();
        return response()->json($stores);
    }

    public function store(NewStoreRequest $request)
    {
        $group = session('group');
        if (!$group and !$request->ajax()) {
            abort(401);
        }

        $store = new Store;
        $store->name = $request['name'];
        $store->address = $request['address'];
        $store->tel = $request['tel'];
        $store->password = $request['password'];
        $store->has_network = $request['has_network'];
        $store->group_id = session('group')->id;
        $store->save();
        return response()->json($store);
    }

    public function update(UpdateStoreRequest $params, Store $store)
    {
        if (!$store) {
            abort(404);
        }

        $group = $store->group()->first();
        if ($group->id != session('group')->id) {
            abort(401);
        }

        $notChange = $this->isStoreNotChanged($store, $params);
        if ($notChange) {
            return response()->json($store);
        } else {
            $store->name = $params['name'];
            $store->address = $params['address'];
            $store->tel = $params['tel'];
            $store->password = $params['password'];
            $store->has_network = $params['has_network'];
            $store->save();
        }

        return response()->json($store);
    }

    private function isStoreNotChanged($store, $params)
    {
        $networkParam = 0;
        if ($params['has_network'] == 'false')
            $networkParam = 0;
        else if ($params['has_network'] == 'true')
            $networkParam = 1;
        if ($store->name != $params['name'])
            return false;
        if ($store->address != $params['address'])
            return false;
        if ($store->tel != $params['tel'])
            return false;
        if ($store->password != $params['password'])
            return false;
        if ($store->has_network != $networkParam)
            return false;
        return true;
    }
}
