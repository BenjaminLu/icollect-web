<?php namespace App\Http\Controllers;

use App\Activity;
use App\Ticket;
use Illuminate\Support\Facades\Auth;

class UserHomeController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        flash()->message('您可以在這裡查詢您的票卷!');
        session()->forget('new_tickets_number');
        if (Auth::check()) {
            $user = Auth::user();
            $id = $user->id;
            $unusedTickets = Ticket::where('user_id', '=', $id)->where('status', '=', 0)->get();
            $usedTickets = Ticket::where('user_id', '=', $id)->where('status', '=', 1)->get();
            $unusedTicketsActivities = null;
            $usedTicketsActivities = null;
            if ($unusedTickets) {
                $idList = null;
                foreach ($unusedTickets as $ticket) {
                    $idList[] = $ticket->activity_id;
                }
                $unusedTicketsActivities = Activity::whereIn('id', $idList)->get();
            }

            if ($usedTickets) {
                $idList = null;
                foreach ($usedTickets as $ticket) {
                    $idList[] = $ticket->activity_id;
                }
                $usedTicketsActivities = Activity::whereIn('id', $idList)->get();
            }

            return view('user.home')
                ->with('unusedTicketsActivities', $unusedTicketsActivities)
                ->with('usedTicketsActivities', $usedTicketsActivities)
                ->with('unusedTickets', $unusedTickets)
                ->with('usedTickets', $usedTickets);
        }
        return view('user.home');
    }
}
