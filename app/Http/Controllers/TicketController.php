<?php namespace App\Http\Controllers;

use App\Activity;
use App\ExRule;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Jobs\SendGiftEmail;
use App\Jobs\SendNonMemberGiftEmail;
use App\Jobs\SendNonMemberTicketEmail;
use App\Jobs\SendReminderEmail;
use App\QRCodeControl;
use App\RSAKey;
use App\Ticket;
use App\TicketGift;
use App\User;
use Carbon\Carbon;
use HttpRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TicketController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $requestTickets = $request->json('tickets');
            $isTicketOpening = false;
            try {
                DB::beginTransaction();
                $newTicketNumber = 0;
                $wantToBuy = 0;
                $totalTickets = 0;
                $tickets = null;
                $ticketIds = null;
                $isTicketOpening = false;
                foreach ($requestTickets as $requestTicket) {
                    $amount = $requestTicket['amount'];
                    $exRuleId = $requestTicket['exchange_rule_id'];
                    $amount = (int)$amount;

                    $exRule = ExRule::where('id', '=', $exRuleId)->first();
                    
                    $quantity = $exRule->quantity;
                    $totalTickets = $quantity;
                    $wantToBuy = $amount;

                    if ($amount > 10 || $amount > $quantity) {
                        continue;
                    }

                    if ($exRule && $quantity > 0) {
                        $activity = $exRule->activity()->first();
                        Carbon::setLocale('zh-TW');
                        $gainStart = Carbon::parse($activity->gain_start);
                        $gainEnd = Carbon::parse($activity->gain_end)->addDay();
                        $now = Carbon::now();
                        $gainDiffForHuman = $gainEnd->diffForHumans($now);
                        if ($now->gte($gainStart) && $now->lt($gainEnd)) {
                            $isTicketOpening = true;
                            for ($i = 0; $i < $amount; $i++) {
                                $ticket = new Ticket();
                                $ticket->name = $exRule->name;
                                $ticket->body = $exRule->body;
                                $ticket->image = $exRule->image;
                                $ticket->activity_id = $activity->id;
                                $ticket->user_id = $user->id;
                                $ticket->status = 0;
                                $ticket->save();
                                $tickets[] = $ticket;
                                $newTicketNumber++;
                                $ticketIds[] = $ticket->id;
                            }
                            $exRule->quantity = $quantity - $newTicketNumber;
                            $exRule->save();
                        } 
                    }
                }
                DB::commit();
            } catch(Exception $e) {
                DB::rollback();
            }
            if($isTicketOpening) {
                if ($newTicketNumber > 0) {
                    $response = [
                        'status' => true
                    ];
                    $lastNumber = session()->get('new_tickets_number');
                    $newNumber = $lastNumber + $newTicketNumber;
                    $request->session()->put('new_tickets_number', $newNumber);

                    $ticket = $tickets[0];
                    $activity = $ticket->activity()->first();
                    $this->dispatch(new SendReminderEmail(json_encode($tickets), $user, $activity));

                    return response()->json($response);
                } else if($wantToBuy > $totalTickets) {
                    $response = [
                        'status' => false,
                        'message' => '購買失敗：超過票卷數量'
                    ];
                    return response()->json($response);
                } else {
                    $response = [
                        'status' => false,
                        'message' => '沒有取得任何票卷'
                    ];
                    return response()->json($response);
                }
            } else {
                $response = [
                        'status' => false,
                        'message' => '取票時間結束'
                ];
                return response()->json($response);
            }
        }

        return abort(401);
    }

    public function qrcodeImage($qrcodeid, $token)
    {
        $qrcode = QRCodeControl::where('id', '=', $qrcodeid)->first();
        if ($qrcode and (strcmp($token, $qrcode->token) == 0)) {
            $image = storage_path("qrcode/" . $qrcode->user_id . "/" . $qrcode->id . '.png');
            if (!file_exists($image)) {
                return abort(404);
            }
            $returnImage = Image::make($image);
            return $returnImage->response();
        }
        return abort(404);
    }

    private function generateRandomString($length = 30)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getTickets()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $ticketsJson = file_get_contents("http://" . config('app.tomcat_host') . ":" . config('app.tomcat_port') . "/icollect/sign/user/" . $user->id . "/ticket/");
            $tickets = json_decode($ticketsJson);
            if (count($tickets) > 0) {
                return response()->json($tickets);
            }
        }

        return response()->json(array());
    }

    public function getTicketActivityList()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $activityIds = Ticket::where('user_id', '=', $user->id)->lists('activity_id');
            $activities = Activity::whereIn('id', $activityIds)->get()->toArray();
            return response()->json($activities);
        }

        return response()->json(array());
    }

    public function getPublicKey()
    {
        $publicKey = RSAKey::where('type', '=', 'public')->orderBy('id', 'desc')->first();
        $response = array(
            'modulus' => $publicKey->modulus,
            'exponent' => $publicKey->exponent
        );

        return response()->json($response);
    }

    public function disableTicket(Request $request)
    {
        $ticketID = $request['id'];
        $ticket = Ticket::where('id', '=', $ticketID)->first();
        if ($ticket->status) {
            $response = array(
                'status' => false,
                'message' => '該票卷已使用過，請勿重複使用'
            );
            return response()->json($response);
        }

        if ($this->storeCheck()) {
            $store = session()->get('store');
            $group = $store->group()->first();
            $groupFromTicket = $ticket->activity()->first()->group()->first();
            if ($group->id == $groupFromTicket->id) {
                $ticket->status = true;
                $ticket->save();
                $response = array(
                    'status' => true,
                    'message' => '成功核銷票卷'
                );
                return response()->json($response);
            }
            $response = array(
                'status' => false,
                'message' => '店家資料不符，無權限修改票卷'
            );
            return response()->json($response);
        }
        $response = array(
            'status' => false,
            'message' => '未登入'
        );
        return response()->json($response);
    }

    public function bulkDisableTicket(Request $request)
    {
        if ($this->storeCheck()) {
            $store = session()->get('store');
            $tidJsonArrayString = $request['tids'];
            $ticketIds = json_decode($tidJsonArrayString);
            $tickets = Ticket::whereIn('id', $ticketIds)->get();
            $ticketsStatusArray = null;

            $groupIds = null;
            if (count($tickets) > 0) {
                foreach ($tickets as $ticket) {
                    $group = $ticket->activity()->first()->group()->first();
                    $groupIds[] = $group->id;
                }
            }
            $groupIds = array_unique($groupIds);
            if (count($groupIds) != 1) {
                return abort(401);
            }

            $groupOfStore = $store->group()->first();
            if ($groupIds[0] != $groupOfStore->id) {
                return abort(401);
            }
            try {
                DB::beginTransaction();
                if (count($ticketIds) > 0) {
                    foreach ($tickets as $ticket) {
                        if ($ticket->status) {
                            $user = $ticket->user()->first();
                            $ticketsStatus = array(
                                'tid' => $ticket->id,
                                'status' => false,
                                'message' => "核銷失敗，票券已被使用，請盡速連絡該消費者求償",
                                'user_name' => $user->name,
                                "user_email" => $user->email,
                            );
                            $ticketsStatusArray[] = $ticketsStatus;
                        } else {
                            $ticketsStatus = array(
                                'tid' => $ticket->id,
                                'status' => true,
                                'message' => "核銷成功",
                            );
                            $ticketsStatusArray[] = $ticketsStatus;
                            $ticket->status = true;
                            $ticket->save();
                        }
                    }
                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
            }
            return response()->json($ticketsStatusArray);
        }

        return abort(401);
    }

    private function storeCheck()
    {
        return (session()->get('store_check') == true);
    }

    public function sendGift(Request $request)
    {
        if (Auth::check()) {
            $requestTickets = $request->json('tickets');
            $giftMessage = $request->json('message');
            $receiverEmail = $request->json('receiver_email');
            $sender = Auth::user();
            $receiver = User::where('email', '=', $receiverEmail)->first();
            $ticketIds = null;
            $isTicketOpening = false;

            if ($receiver) {
                try {
                    DB::beginTransaction();
                    $newTicketNumber = 0;
                    $tickets = null;
                    foreach ($requestTickets as $requestTicket) {
                        $amount = $requestTicket['amount'];
                        $exRuleId = $requestTicket['exchange_rule_id'];
                        $amount = (int)$amount;

                        $exRule = ExRule::where('id', '=', $exRuleId)->first();

                        $quantity = $exRule->quantity;

                        if ($amount > 10 || $amount > $quantity) {
                            continue;
                        }

                        if ($exRule && $quantity > 0) {
                            $activity = $exRule->activity()->first();
                            Carbon::setLocale('zh-TW');
                            $gainStart = Carbon::parse($activity->gain_start);
                            $gainEnd = Carbon::parse($activity->gain_end)->addDay();
                            $now = Carbon::now();
                            $gainDiffForHuman = $gainEnd->diffForHumans($now);
                            if ($now->gte($gainStart) && $now->lt($gainEnd)) {
                                $isTicketOpening = true;
                                for ($i = 0; $i < $amount; $i++) {
                                    $ticket = new Ticket();
                                    $ticket->name = $exRule->name;
                                    $ticket->body = $exRule->body;
                                    $ticket->image = $exRule->image;
                                    $ticket->activity_id = $activity->id;
                                    $ticket->user_id = $receiver->id;
                                    $ticket->status = 0;
                                    $ticket->save();
                                    $tickets[] = $ticket;
                                    $ticketIds[] = $ticket->id;
                                    $newTicketNumber++;
                                }
                                $exRule->quantity = $quantity - $newTicketNumber;
                                $exRule->save();
                            }
                        }
                    }
                    if($isTicketOpening) {
                        $ticketGift = new TicketGift;
                        $ticketGift->sender_id = $sender->id;
                        $ticketGift->receiver_id = $receiver->id;
                        $ticketGift->ticket_ids = json_encode($ticketIds);
                        $ticketGift->message = $giftMessage;
                        $ticketGift->save();
                    }
                    DB::commit();
                } catch (Exception $e) {
                    DB:rollback();
                }
                if($isTicketOpening) {
                    if ($newTicketNumber > 0) {
                        $response = [
                            'status' => true
                        ];

                        $firstTicket = $tickets[0];
                        $activity = $firstTicket->activity()->first();
                        $this->dispatch(new SendGiftEmail(json_encode($tickets), $sender, $receiver, $giftMessage, $activity));
                        return response()->json($response);
                    }
                } else {
                    $response = [
                        'status' => false,
                        'message' => '取票時間結束'
                    ];
                    return response()->json($response);
                }
                
            } else {

                try {
                    DB::beginTransaction();
                    $newTicketNumber = 0;
                    $tickets = null;
                    foreach ($requestTickets as $requestTicket) {
                        $amount = $requestTicket['amount'];
                        $exRuleId = $requestTicket['exchange_rule_id'];
                        $amount = (int)$amount;

                        $exRule = ExRule::where('id', '=', $exRuleId)->first();

                        $quantity = $exRule->quantity;

                        if ($amount > 10 || $amount > $quantity) {
                            continue;
                        }

                        if ($exRule) {
                            $activity = $exRule->activity()->first();
                            Carbon::setLocale('zh-TW');
                            $gainStart = Carbon::parse($activity->gain_start);
                            $gainEnd = Carbon::parse($activity->gain_end)->addDay();
                            $now = Carbon::now();
                            $gainDiffForHuman = $gainEnd->diffForHumans($now);
                            if ($now->gte($gainStart) && $now->lt($gainEnd)) {
                                $isTicketOpening = true;
                                for ($i = 0; $i < $amount; $i++) {
                                    $ticket = new Ticket();
                                    $ticket->name = $exRule->name;
                                    $ticket->body = $exRule->body;
                                    $ticket->image = $exRule->image;
                                    $ticket->activity_id = $activity->id;
                                    $ticket->user_id = 0;
                                    $ticket->status = 0;
                                    $ticket->save();
                                    $tickets[] = $ticket;
                                    $ticketIds[] = $ticket->id;
                                    $newTicketNumber++;
                                }
                                $exRule->quantity = $quantity - $newTicketNumber;
                                $exRule->save();
                            }
                        }
                    }
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                }
                if($isTicketOpening) {
                    if ($newTicketNumber > 0) {
                        $response = [
                            'status' => true
                        ];
                        $ticket = $tickets[0];
                        $activity = $ticket->activity()->first();
                        $this->dispatch(new SendNonMemberGiftEmail(json_encode($tickets), $sender, $receiverEmail, $giftMessage, $activity));
                        return response()->json($response);
                    }
                } else {
                    $response = [
                        'status' => false,
                        'message' => '取票時間結束'
                    ];
                    return response()->json($response);
                }   
            }
        }

        $response = [
            'status' => false
        ];
        return response()->json($response);
    }

    public function receivedGift()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $ticketGifts = TicketGift::where('receiver_id', '=', $user->id)->orderBy('created_at', 'dsec')->get();
            foreach ($ticketGifts as $ticketGift) {
                $sender = User::where('id', '=', $ticketGift->sender_id)->first();
                $tickets = Ticket::whereIn('id', json_decode($ticketGift->ticket_ids))->get();
                $firstTicket = $tickets[0];
                $activity = $firstTicket->activity()->first();
                $ticketGift->tickets = $tickets;
                $ticketGift->sender = $sender;
                $ticketGift->activity = $activity;
            }
            return view('user.gift')->with('ticketGifts', $ticketGifts);
        }

        return abort(401);
    }

    public function sellTicketFromStore(Request $request)
    {
        if ($this->storeCheck()) {
            $store = session()->get('store');
            $email = $request->json('email');
            $requestTickets = $request->json('tickets');
            $newTicketNumber = 0;
            $tickets = null;
            $ticketIds = null;
            if (count($requestTickets) > 0) {
                $receiver = User::where('email', '=', $email)->first();
                if ($receiver) {
                    try {
                        //Member
                        DB::beginTransaction();
                        foreach ($requestTickets as $requestTicket) {
                            $exRuleId = $requestTicket['ex_rule_id'];
                            $requestTicketNumber = $requestTicket['number'];
                            $exRule = ExRule::where('id', '=', $exRuleId)->first();

                            if ($exRule) {
                                $activity = $exRule->activity()->first();
                                for ($i = 0; $i < $requestTicketNumber; $i++) {
                                    $ticket = new Ticket();
                                    $ticket->name = $exRule->name;
                                    $ticket->body = $exRule->body;
                                    $ticket->image = $exRule->image;
                                    $ticket->activity_id = $activity->id;
                                    $ticket->user_id = $receiver->id;
                                    $ticket->status = 0;
                                    $ticket->save();
                                    $tickets[] = $ticket;
                                    $ticketIds[] = $ticket->id;
                                    $newTicketNumber++;
                                }
                            }
                        }
                        DB::commit();
                    } catch (Exception $e) {
                        DB::rollback();
                    }
                    
                    if ($newTicketNumber > 0) {
                        $response = [
                            'status' => true,
                            'message' => '販售成功'
                        ];

                        $ticket = $tickets[0];
                        $activity = $ticket->activity()->first();
                        $this->dispatch(new SendReminderEmail(json_encode($tickets), $receiver, $activity));

                        return response()->json($response);
                    } else {
                        $response = [
                            'status' => false,
                            'message' => '伺服器資料無法寫入，請稍後再試'
                        ];
                        return response()->json($response);
                    }
                } else {
                    //Non-Member
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $response = [
                            'status' => false,
                            'message' => 'Email格式錯誤'
                        ];
                        return response()->json($response);
                    }

                    try {
                        DB::beginTransaction();
                        foreach ($requestTickets as $requestTicket) {
                            $exRuleId = $requestTicket['ex_rule_id'];
                            $requestTicketNumber = $requestTicket['number'];
                            $exRule = ExRule::where('id', '=', $exRuleId)->first();

                            if ($exRule) {
                                $activity = $exRule->activity()->first();
                                for ($i = 0; $i < $requestTicketNumber; $i++) {
                                    $ticket = new Ticket();
                                    $ticket->name = $exRule->name;
                                    $ticket->body = $exRule->body;
                                    $ticket->image = $exRule->image;
                                    $ticket->activity_id = $activity->id;
                                    $ticket->user_id = 0;
                                    $ticket->status = 0;
                                    $ticket->save();
                                    $tickets[] = $ticket;
                                    $ticketIds[] = $ticket->id;
                                    $newTicketNumber++;
                                }
                            }
                        }

                        DB::commit();
                    } catch (Exception $e) {
                        DB::rollback();
                    }

                    if ($newTicketNumber > 0) {
                        $response = [
                            'status' => true,
                            'message' => '販售成功'
                        ];

                        $ticket = $tickets[0];
                        $activity = $ticket->activity()->first();
                        $this->dispatch(new SendNonMemberTicketEmail(json_encode($tickets), $email, $activity));
                        return response()->json($response);
                    } else {
                        $response = [
                            'status' => false,
                            'message' => '伺服器資料無法寫入，請稍後再試'
                        ];
                        return response()->json($response);
                    }
                }
            } else {
                $response = array(
                    'status' => false,
                    'message' => '沒有要求販賣的票券',
                );

                return response()->json($response);
            }
        }

        $response = array(
            'status' => false,
            'message' => '店家未登入'
        );

        return response()->json($response);
    }

    public function checkDeadline($activity) 
    {
        Carbon::setLocale('zh-TW');
        $gainStart = Carbon::parse($activity->gain_start);
        $gainEnd = Carbon::parse($activity->gain_end)->addDay();
        $now = Carbon::now();
        $gainDiffForHuman = $gainEnd->diffForHumans($now);
        $isTicketOpening = false;
        if ($now->gte($gainStart) && $now->lt($gainEnd)) {
            $isTicketOpening = true;
        }
        return $isTicketOpening;
    }
}
