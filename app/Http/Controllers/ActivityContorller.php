<?php namespace App\Http\Controllers;

use App\ExRule;
use App\GainRule;
use App\Group;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\NewActivityRequest;
use App\Activity;
use App\Http\Requests\UpdateActivityRequest;
use App\Store;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Session;
use Log;

class ActivityController extends Controller
{

    public function __construct()
    {
        $this->middleware('group_auth', ['except' => ['show', 'all', 'search']]);
    }

    public function index()
    {
        $group = session('group');
        $activities = $group->activities()->orderBy('created_at', 'desc')->get();
        flash()->message('您可以在這裡管理您的活動!');
        return view('activity.home')->with('activities', $activities);
    }

    public function all($page)
    {
        $itemPerPage = 11;
        $skip = ($page - 1) * $itemPerPage;
        $activitiesNumber = Activity::count();
        $lastPage = (int)($activitiesNumber / $itemPerPage);
        $remainder = $activitiesNumber % $itemPerPage;

        if ($remainder > 0) {
            $lastPage++;
        }
        $activities = Activity::orderBy('created_at', 'desc')->skip($skip)->take($itemPerPage)->get();
        return view('activity.all')
            ->with('activities', $activities)
            ->with('page', $page)
            ->with('lastPage', $lastPage);
    }

    public function search($keyword, $type, $page)
    {
        if (!empty($keyword) and !empty($type) and $page >= 1) {
            $itemPerPage = 11;
            $skip = ($page - 1) * $itemPerPage;
            if ($type == "活動") {
                $activities = Activity::where('name', 'LIKE', '%' . $keyword . '%')
                    ->groupBy('name')
                    ->skip($skip)
                    ->take($itemPerPage)
                    ->orderBy('created_at', 'desc')->get();
                $activitiesNumber = count($activities);
                $lastPage = (int)($activitiesNumber / $itemPerPage);
                $remainder = $activitiesNumber % $itemPerPage;

                if ($remainder > 0) {
                    $lastPage++;
                }
                return view('activity.all')
                    ->with('activities', $activities)
                    ->with('page', $page)
                    ->with('lastPage', $lastPage);
            } else if ($type == "企業") {
                $groups = Group::where('name', 'LIKE', '%' . $keyword . '%')->distinct()->get();
                $groupIds = array();
                if (count($groups) > 0) {
                    for ($i = 0; $i < count($groups); $i++) {
                        $groupIds[$i] = $groups[$i]->id;
                    }
                    $activities = Activity::whereIn('group_id', $groupIds)
                        ->skip($skip)
                        ->take($itemPerPage)
                        ->orderBy('created_at', 'desc')->get();
                    $activitiesNumber = count($activities);
                    $lastPage = (int)($activitiesNumber / $itemPerPage);
                    $remainder = $activitiesNumber % $itemPerPage;

                    if ($remainder > 0) {
                        $lastPage++;
                    }
                    return view('activity.all')
                        ->with('activities', $activities)
                        ->with('page', $page)
                        ->with('lastPage', $lastPage);
                }
            } else if ($type == "地址") {
                $stores = Store::where('address', 'LIKE', '%' . $keyword . '%')->get();
                if (count($stores) > 0) {
                    $groupIds = array();
                    $i = 0;
                    foreach ($stores as $store) {
                        $group = $store->group()->first();
                        $groupIds[$i] = $group->id;
                        $i++;
                    }
                    $activities = Activity::whereIn('group_id', $groupIds)
                        ->skip($skip)
                        ->take($itemPerPage)
                        ->orderBy('created_at', 'desc')->get();
                    $activitiesNumber = count($activities);
                    $lastPage = (int)($activitiesNumber / $itemPerPage);
                    $remainder = $activitiesNumber % $itemPerPage;

                    if ($remainder > 0) {
                        $lastPage++;
                    }
                    return view('activity.all')
                        ->with('activities', $activities)
                        ->with('page', $page)
                        ->with('lastPage', $lastPage);
                }
            } else if ($type == "票卷") {
                $exRules = ExRule::where('name', 'LIKE', '%' . $keyword . '%')->get();
                if (count($exRules) > 0) {
                    $activitiesIds = array();
                    $i = 0;
                    foreach ($exRules as $exRule) {
                        $activitiesIds[$i] = $exRule->activity_id;
                        $i++;
                    }
                    $activitiesIds = array_unique($activitiesIds);
                    $activities = Activity::whereIn('id', $activitiesIds)
                        ->skip($skip)
                        ->take($itemPerPage)
                        ->orderBy('created_at', 'desc')->get();
                    $activitiesNumber = count($activities);
                    $lastPage = (int)($activitiesNumber / $itemPerPage);
                    $remainder = $activitiesNumber % $itemPerPage;

                    if ($remainder > 0) {
                        $lastPage++;
                    }
                    return view('activity.all')
                        ->with('activities', $activities)
                        ->with('page', $page)
                        ->with('lastPage', $lastPage);
                }
            } else {
                return abort(404);
            }
        }

        return abort(404);
    }

    public function show($aid)
    {
        $activity = Activity::find($aid);
        if (!$activity) {
            abort(404);
        }

        $group = $activity->group()->first();
        $stores = $group->stores()->get();
        $gainRules = $activity->gainRules()->orderBy('value', 'asc')->get();
        $exRules = $activity->exRules()->orderBy('id', 'asc')->get();
        return view('activity.show')
            ->with('activity', $activity)
            ->with('group', $group)
            ->with('exRules', $exRules);
    }

    public function preview(Request $request)
    {
        if($request->session()->get('group_check')) {
            $group = session('group');
            $activity = new Activity();
            $activity->name = $request['name'];
            $activity->body = $request['body'];
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $image_name = $this->generateImageName($file);
                $this->saveImage($file, $group, $image_name);
                $activity->image = $this->generateImagePathInDB($group, $image_name);
            } else {
                $activity->image = $request['image_url'];
            }
            $activity->location = $request['location'];
            $activity->gain_start = $request['gain_start'];
            $activity->gain_end = $request['gain_end'];
            $activity->ex_start = $request['ex_start'];
            $activity->ex_end = $request['ex_end'];

            $group = session('group');

            $exRules = $request['ex_rules'];
            return view('activity.preview')
                ->with('activity', $activity)
                ->with('group', $group)
                ->with('exRules', $exRules);
        }
       return abort(401);
    }

    public function create()
    {
        flash()->message('您可以在這裡新增您的活動!');
        return view('activity.add');
    }

    public function store(NewActivityRequest $request)
    {
        $group = session('group');
        if (!$group) {
            abort(401);
        }

        $activity = new Activity;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $image_name = $this->generateImageName($file);
            $this->saveImage($file, $group, $image_name);
            $activity->image = $this->generateImagePathInDB($group, $image_name);
        }

        $activity->name = $request['name'];
        $activity->body = $request['body'];
        $activity->location = $request['location'];
        $activity->gain_start = $request['gain_start'];
        $activity->gain_end = $request['gain_end'];
        $activity->ex_start = $request['ex_start'];
        $activity->ex_end = $request['ex_end'];
        $activity->group_id = $group->id;
        $activity->save();

        DB::beginTransaction();
        $gain_rules = $request['gain_rules'];
        $ex_rules = $request['ex_rules'];
        if ($gain_rules) {
            foreach ($gain_rules as $gain_rule) {
                if (!empty($gain_rule['body']) and !empty($gain_rule['value'])) {
                    $gainRule = new GainRule;
                    $gainRule->body = $gain_rule['body'];
                    $gainRule->value = $gain_rule['value'];
                    $gainRule->activity_id = $activity->id;
                    $gainRule->save();
                }
            }
        }

        if ($ex_rules) {
            foreach ($ex_rules as $ex_rule) {
                $quantity = intval($ex_rule['quantity']);
                $quantityCheck = false;

                if($quantity > 0) {
                    $quantityCheck = true;
                }

                if(!empty( $ex_rule['name']) and !empty($ex_rule['body']) and $quantityCheck) {
                    $exRule = new ExRule;
                    $exRule->name = $ex_rule['name'];
                    $exRule->body = $ex_rule['body'];
                    $exRule->quantity = $ex_rule['quantity'];
                    $exRule->image = $ex_rule['image'];
                    $exRule->activity_id = $activity->id;
                    $exRule->save();
                }
            }
        }
        DB::commit();

        Redis::publish('activity-published', json_encode([
            'activity_id' => $activity->id,
            'url' => url('/activity/' . $activity->id),
            'image' => url('/' . $activity->image),
            'name' => $activity->name,
            'body' => $activity->body
        ]));
        return redirect('/activity');
    }

    public function edit($aid)
    {
        $group = session('group');
        $activity = $group->activities()->where('id', '=', $aid)->first();
        if (!$activity) {
            abort(404);
        }
        $gainRules = $activity->gainRules()->get();
        $exRules = $activity->exRules()->get();
        flash()->message('您可以在這裡編輯您的活動!');
        return view('activity.edit')->with('activity', $activity)->with('gainRules', $gainRules)->with('exRules', $exRules);
    }

    public function update(UpdateActivityRequest $request, $aid)
    {
        $groupInSession = session('group');
        //eager loading
        $activity = Activity::with('gainRules', 'exRules', 'group')->where('id', '=', $aid)->first();
        $group = $activity['group'];
        if ($groupInSession->id != $group->id) {
            abort(401);
        }

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $image_name = $this->generateImageName($file);
            $this->saveImage($file, $group, $image_name);
            $activity->image = $this->generateImagePathInDB($group, $image_name);
        }

        $activity->name = $request['name'];
        $activity->body = $request['body'];
        $activity->location = $request['location'];
        $activity->gain_start = $request['gain_start'];
        $activity->gain_end = $request['gain_end'];
        $activity->ex_start = $request['ex_start'];
        $activity->ex_end = $request['ex_end'];
        $activity->save();

        $originalGainRules = $activity['gainRules'];
        $originalExRules = $activity['exRules'];

        DB::beginTransaction();
        $gain_rules = $request['gain_rules'];
        $keepGainRulesIDs = array();
        $ex_rules = $request['ex_rules'];
        $keepExRulesIDs = array();

        //update gain rules
        if ($gain_rules) {
            foreach ($gain_rules as $gain_rule) {
                if ($gain_rule['id']) {
                    //update the existing gain rule
                    $id = $gain_rule['id'];
                    foreach ($originalGainRules as $originalGainRule) {
                        if ($originalGainRule->id == $id) {
                            if (!empty($gain_rule['body']) and !empty($gain_rule['value'])) {
                                $originalGainRule->body = $gain_rule['body'];
                                $originalGainRule->value = $gain_rule['value'];
                                $originalGainRule->save();
                            }
                            array_push($keepGainRulesIDs, $originalGainRule->id);
                            break;
                        }
                    }
                } else {
                    if (!empty($gain_rule['body']) and !empty($gain_rule['value'])) {
                        $gainRule = new GainRule;
                        $gainRule->body = $gain_rule['body'];
                        $gainRule->value = $gain_rule['value'];
                        $gainRule->activity_id = $activity->id;
                        $gainRule->save();
                    }
                }
            }
        }

        //delete the useless original gain rules
        foreach ($originalGainRules as $originalGainRule) {
            $id = $originalGainRule->id;
            $needToDelete = true;
            foreach ($keepGainRulesIDs as $safeId) {
                if ($id == $safeId) {
                    $needToDelete = false;
                }
            }

            if ($needToDelete) {
                $originalGainRule->delete();
            }
        }

        //update exchange rule
        if ($ex_rules) {
            foreach ($ex_rules as $ex_rule) {
                if ($ex_rule['id']) {
                    $id = $ex_rule['id'];
                    foreach ($originalExRules as $originalExRule) {
                        if ($originalExRule->id == $id) {
                            $quantity = intval($ex_rule['quantity']);
                            $quantityCheck = false;

                            if ($quantity >= 0) {
                                $quantityCheck = true;
                            }

                            if (!empty($ex_rule['name']) and !empty($ex_rule['body']) and $quantityCheck) {
                                $originalExRule->name = $ex_rule['name'];
                                $originalExRule->body = $ex_rule['body'];
                                $originalExRule->quantity = $ex_rule['quantity'];
                                $originalExRule->image = $ex_rule['image'];
                                $originalExRule->save();
                            }
                            array_push($keepExRulesIDs, $originalExRule->id);
                            break;
                        }
                    }
                } else {
                    $quantity = intval($ex_rule['quantity']);
                    $quantityCheck = false;

                    if ($quantity >= 0) {
                        $quantityCheck = true;
                    }

                    if (!empty($ex_rule['name']) and !empty($ex_rule['body']) and $quantityCheck) {
                        $exRule = new ExRule;
                        $exRule->name = $ex_rule['name'];
                        $exRule->body = $ex_rule['body'];
                        $exRule->quantity = $ex_rule['quantity'];
                        $exRule->image = $ex_rule['image'];
                        $exRule->activity_id = $activity->id;
                        $exRule->save();
                    }
                }
            }
        }

        //delete the useless original exchange rules
        foreach ($originalExRules as $originalExRule) {
            $id = $originalExRule->id;
            $needToDelete = true;
            foreach ($keepExRulesIDs as $safeId) {
                if ($id == $safeId) {
                    $needToDelete = false;
                }
            }

            if ($needToDelete) {
                $originalExRule->delete();
            }
        }

        DB::commit();
        return redirect('/activity');
    }

    public function photoUpload(Request $request)
    {
        $group = session('group');
        if ($request->hasFile('gift-image')) {
            $file = $request->file('gift-image');
            $image_name = $this->generateImageName($file);
            $this->saveImage($file, $group, $image_name);
            return response()->json(['url' => $this->generateImagePathInDB($group, $image_name)]);
        }
    }

    private function generateImageName($file)
    {
        return time() . "." . $file->getClientOriginalExtension();
    }

    private function saveImage($file, $group, $image_name)
    {
        $file->move('image/' . $group->id, $image_name);
        Image::make(sprintf('image/' . $group->id . '/%s', $image_name))->save();
    }

    private function generateImagePathInDB($group, $image_name)
    {
        return 'image/' . $group->id . '/' . $image_name;
    }
}
