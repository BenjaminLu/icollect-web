<?php namespace App\Http\Controllers;

use App\Activity;
use App\Group;
use App\Http\Requests\CreateGroupRequest;
use App\Http\Requests\LoginGroupRequest;
use App\Ticket;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Facades\Excel;
use Session;

class GroupController extends Controller
{

    public function __construct()
    {
        $this->middleware('group_auth', ['except' => ['getRegister', 'getLogin', 'postRegister', 'postLogin']]);
    }

    public function getRegister()
    {
        if ($this->groupCheck()) {
            return redirect('/group/home');
        }

        return view('group.register');
    }

    public function getLogin()
    {
        if ($this->groupCheck()) {
            return redirect('/group/home');
        }

        return view('group.login');
    }

    public function home()
    {
        if (!$this->groupCheck()) {
            return redirect('/group/login');
        }

        flash()->message('您可以在這裡管理您的驗票裝置!');
        return view('group.home');
    }

    /**
     * sign up a Group
     * @param CreateGroupRequest $request
     * @return \Illuminate\View\View
     */
    public function postRegister(CreateGroupRequest $request)
    {
        if ($this->groupCheck()) {
            return redirect('/group/home');
        }

        $params = $request->all();
        $group = $this->insertNewGroup($params);
        $this->loginGroup($group, $request);

        flash()->success("帳號創立成功！，開始社團營業吧！");
        return view('group.home');
    }

    public function postLogin(LoginGroupRequest $request)
    {
        if ($this->groupCheck()) {
            return redirect('/group/home');
        }

        $params = $request->all();
        $group = $this->findGroupByEmail($params['email']);

        if (!$group) {
            return redirect('/group/login')->withInput($request->all())->withErrors(['errors' => "帳號或密碼錯誤"]);
        }

        $isValidGroup = $this->checkGroupPassword($params['password'], $group->password);
        if (!$isValidGroup) {
            return redirect('/group/login')->withInput($request->all())->withErrors(['errors' => "帳號或密碼錯誤"]);
        }

        $this->loginGroup($group, $request);
        flash()->success("登入成功，您可以在這裡管理您的社團！");
        return view('group.home');
    }

    public function logout()
    {
        $this->logoutGroup();
        return redirect('/');
    }

    private function insertNewGroup($params)
    {
        $group = new Group;
        $group->name = $params['name'];
        $group->email = $params['email'];
        $group->password = Hash::make($params['password']);
        $group->address = $params['address'];
        $group->tel = $params['tel'];
        $group->type = $params['type'];
        $group->save();
        return $group;
    }

    private function findGroupByEmail($email)
    {
        $group = Group::where('email', $email)->first();
        return $group;
    }

    private function checkGroupPassword($plainPass, $passwordInDB)
    {
        return Hash::check($plainPass, $passwordInDB);
    }

    private function groupCheck()
    {
        return (session()->get('group_check') == true);
    }

    private function loginGroup($group, $request)
    {
        $request->session()->put('group_check', true);
        $request->session()->put('group', $group);
    }

    private function logoutGroup()
    {
        session()->forget('group_check');
        session()->forget('group');
    }

    public function dashboard($activityId)
    {
        $group = session()->get('group');
        $activity = Activity::where('id', '=', $activityId)->first();
        $groupFromActivity = $activity->group()->first();
        if ($groupFromActivity->id == $group->id) {
            $pageViewInRedis = Redis::get('activity.pageview.' . $activity->id);
            $pageView = 0;
            if ($pageViewInRedis) {
                $pageView = $pageViewInRedis;
            }

            return view('activity.dashboard')
                ->with('activity', $activity)
                ->with('pageView', $pageView);
        }
        return abort(401);
    }

    public function dashboardHistogramData($activityId)
    {
        $group = session()->get('group');
        $activity = Activity::where('id', '=', $activityId)->first();
        $groupFromActivity = $activity->group()->first();

        if ($activity and ($groupFromActivity->id == $group->id)) {
            $aid = $activity->id;
            $tickets = Ticket::where('activity_id', '=', $aid)->orderBy('created_at', 'asc')->get();

            if (count($tickets) > 0) {
                $items = null;
                foreach ($tickets as $ticket) {
                    $formatString = Carbon::createFromFormat('Y-m-d H:i:s', $ticket->created_at);
                    $date = $formatString->toDateString();
                    $items[$date][$ticket->name][] = $ticket;
                    $items[$date]['State'] = $date;
                }
                $response = null;
                foreach ($items as $item) {
                    foreach ($item as $key => $value) {
                        if ($key != 'State') {
                            $count = count($value);
                            $item[$key] = $count;
                        }
                    }
                    $response[] = $item;
                }
                return response()->json($response);
            } else {
                return response()->json(array());
            }
        } else {
            return response()->json(array());
        }
    }

    public function dashboardAmountPieData($activityId)
    {
        $group = session()->get('group');
        $activity = Activity::where('id', '=', $activityId)->first();
        $groupFromActivity = $activity->group()->first();

        if ($activity and ($groupFromActivity->id == $group->id)) {
            $aid = $activity->id;
            $tickets = Ticket::select(DB::raw('name as label, count(name) as instances'))->where('activity_id', '=', $aid)->groupBy('name')->get();
            return response()->json($tickets);
        } else {
            return response()->json(array());
        }
    }

    public function lookupTicketReport($aid)
    {
        $group = session()->get('group');
        $activity = Activity::find($aid)->first();
        if($activity) {
            if($activity->group()->first()->id == $group->id) {
                $tickets = Ticket::where('activity_id', $aid)->get();
                $userIds = $tickets->pluck('user_id')->toArray();
                $userIds = array_unique($userIds);
                $users = User::whereIn('id', $userIds)->get();
                $reports = null;
                foreach($tickets as $ticket) {
                    $taker = null;
                    foreach($users as $user) {
                        if($user->id == $ticket->user_id) {
                            $taker = $user;
                            break;
                        }
                    }
                    $ticketStatus = ($ticket->status) ? "已使用" : "未使用";
                    $item = new \stdClass();
                    $item->ticketId = $ticket->id;
                    $item->ticketName = $ticket->name;
                    $item->ticketBody = $ticket->body;
                    $item->isUsed = $ticketStatus;
                    $item->taker = $taker->name;
                    $item->takerEmail = $taker->email;
                    $item->takeTime = $ticket->created_at;
                    $reports[] = $item;
                }
                return view('activity.report')
                    ->with('reports', $reports)
                    ->with('activity', $activity);
            }
        }

        return abort(404);
    }

    public function downloadTicketReport($aid)
    {
        $group = session()->get('group');
        $activity = Activity::find($aid)->first();
        if($activity) {
            if($activity->group()->first()->id == $group->id) {
                $tickets = Ticket::where('activity_id', $aid)->get();
                $userIds = $tickets->pluck('user_id')->toArray();
                $userIds = array_unique($userIds);
                $users = User::whereIn('id', $userIds)->get();
                $export = null;
                foreach($tickets as $ticket) {
                    $taker = null;
                    foreach($users as $user) {
                        if($user->id == $ticket->user_id) {
                            $taker = $user;
                            break;
                        }
                    }
                    $ticketStatus = ($ticket->status) ? "已使用" : "未使用";
                    $export[] = array(
                        '票券ID' => $ticket->id,
                        '票券名稱' => $ticket->name,
                        '票券內容' => $ticket->body,
                        '票券使用情形' => $ticketStatus,
                        '取票人'   => $taker->name,
                        '取票人E-mail' => $taker->email,
                        '取票時間' => $ticket->created_at
                    );
                }

                return Excel::create($activity->name . Carbon::now(), function($excel) use ($export) {
                    $excel->sheet('export', function($sheet) use ($export) {
                        $sheet->fromArray($export);
                    });

                })->export('xls');
            }
        }

        return abort(404);
    }
}
