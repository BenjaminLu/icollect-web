<?php namespace App\Http\Controllers;

use App\Activity;
use App\ExRule;
use App\FavoriteActivity;
use App\GainRule;
use App\Group;
use App\Http\Requests;
use App\Store;
use Auth;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    //Client
    public function userLogin(Request $request)
    {
        $email = $request['email'];
        $password = $request['password'];

        if (Auth::check()) {
            $user = Auth::user();
            $response = array(
                'id' => $user->id,
                'message' => "already login",
                'name' => $user->name,
                'email' => $user->email,
                'success' => true
            );
            return response()->json($response);
        } else {
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                $user = Auth::user();
                $response = array(
                    'id' => $user->id,
                    'message' => "first login",
                    'name' => $user->name,
                    'email' => $user->email,
                    'success' => true,
                );
                return response()->json($response);
            }

            $response = array(
                'message' => "login fail",
                'success' => false
            );
            return response()->json($response);
        }
    }

    public function userLogout()
    {
        Auth::logout();
        $response = array(
            'success' => true
        );
        return response()->json($response);
    }

    public function mobileIndex($page)
    {
        $itemPerPage = 6;
        $skip = ($page - 1) * $itemPerPage;
        $activities = Activity::orderBy('created_at', 'desc')->skip($skip)->take($itemPerPage)->get()->toArray();
        return response()->json($activities);
    }

    public function searchActivity($keyword)
    {
        $activities = Activity::where('name', 'LIKE', '%' . $keyword . '%')->orderBy('created_at', 'desc')->get();
        return response()->json($activities);
    }

    //store api
    public function storeLogin(Request $request)
    {
        $storeId = $request['store_id'];
        $password = $request['password'];
        $response = array();
        if ($this->storeCheck()) {
            $response = array(
                'message' => "Already Login",
                'success' => true
            );
            return response()->json($response);
        } else {
            $store = Store::find($storeId);
            if ($store) {
                if (strcmp($store->password, $password) === 0) {
                    $this->loginStore($store, $request);
                    $response['success'] = true;
                    $response['has_network'] = $store->has_network;
                } else {
                    $response['message'] = "Error Credential";
                    $response['success'] = false;
                }
            } else {
                $response['message'] = "No Store Found";
                $response['success'] = false;
            }
        }
        return response()->json($response);
    }

    public function storeLogout()
    {
        $this->logoutStore();
        $response = array();
        $response['success'] = true;
        return response()->json($response);
    }

    public function storeActivityAll()
    {
        if ($this->storeCheck()) {
            $store = session()->get('store');
            $activities = $store->group()->first()->activities()->orderBy('created_at', 'desc')->get();
            $activitiesIDList = $activities->lists('id');
            $activities = $activities->toArray();
            $gain_rules = GainRule::whereIn('activity_id', $activitiesIDList)->get()->toArray();
            $ex_rules = ExRule::whereIn('activity_id', $activitiesIDList)->get()->toArray();
            foreach ($gain_rules as $gain_rule) {
                $activityID = $gain_rule['activity_id'];
                for ($i = 0; $i < count($activities); $i++) {
                    if ($activities[$i]['id'] == $activityID) {
                        $activities[$i]['gainRules'][] = $gain_rule;
                    }
                }
            }

            foreach ($ex_rules as $ex_rule) {
                $activityID = $ex_rule['activity_id'];
                for ($i = 0; $i < count($activities); $i++) {
                    if ($activities[$i]['id'] == $activityID) {
                        $activities[$i]['exRules'][] = $ex_rule;
                    }
                }
            }

            return response()->json($activities);
        }

        return abort(401);
    }

    private function loginStore($store, $request)
    {
        $request->session()->put('store_check', true);
        $request->session()->put('store', $store);
    }

    private function logoutStore()
    {
        session()->forget('store_check');
        session()->forget('store');
    }

    private function storeCheck()
    {
        return (session()->get('store_check') == true);
    }

    //key word search for all activity auto complete
    public function search($keyword)
    {
        $response = array();
        if ($keyword) {
            $response['success'] = true;
            $groups = Group::where('name', 'LIKE', '%' . $keyword . '%')->groupBy('name')->get()->toArray();
            $activities = Activity::where('name', 'LIKE', '%' . $keyword . '%')->groupBy('name')->take(8)->get()->toArray();
            $stores = Store::where('address', 'LIKE', '%' . $keyword . '%')->groupBy('address')->take(8)->get()->toArray();
            $gifts = ExRule::where('name', 'LIKE', '%' . $keyword . '%')->groupBy('name')->take(8)->get()->toArray();

            if ($activities) {
                $response['results']['category1'] = array('name' => "活動");
                $response['results']['category1']['results'] = array();
                for ($i = 0; $i < count($activities); $i++) {
                    $response['results']['category1']['results'][$i] = array(
                        'title' => $activities[$i]['name']
                    );
                }
            }

            if ($groups) {
                $response['results']['category2'] = array('name' => "企業");
                $response['results']['category2']['results'] = array();
                for ($i = 0; $i < count($groups); $i++) {
                    $response['results']['category2']['results'][$i] = array(
                        'title' => $groups[$i]['name'],
                        'description' => $groups[$i]['type']
                    );
                }
            }

            if ($stores) {
                $response['results']['category3'] = array('name' => "地址");
                $response['results']['category3']['results'] = array();
                for ($i = 0; $i < count($stores); $i++) {
                    $response['results']['category3']['results'][$i] = array(
                        'title' => $stores[$i]['address'],
                        'description' => $stores[$i]['name']
                    );
                }
            }

            if ($gifts) {
                $response['results']['category4'] = array('name' => "票卷");
                $response['results']['category4']['results'] = array();
                for ($i = 0; $i < count($gifts); $i++) {
                    $response['results']['category4']['results'][$i] = array(
                        'title' => $gifts[$i]['name'],
                        'description' => $gifts[$i]['body']
                    );
                }
            }
            return response()->json($response);
        } else {
            $response['success'] = false;
            return response()->json($response);
        }
    }

    public function getTicketsOfActivity($activityId)
    {
        $activity = Activity::where('id', '=', $activityId)->first();
        if ($activity) {
            $exRules = $activity->exRules()->get();
            return response()->json($exRules);
        }

        return response()->json(array());
    }

    public function getStoresOfActivity($activityId)
    {
        $activity = Activity::where('id', '=', $activityId)->first();
        if ($activity) {
            $stores = $activity->group()->first()->stores()->get();

            foreach ($stores as $store) {
                unset($store->password);
            }

            return response()->json($stores);
        }

        return response()->json(array());
    }

    public function addFavoriteActivity($activityId)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $favoriteActivity = FavoriteActivity::where('user_id', '=', $user->id)
                ->where('activity_id', '=', $activityId)->first();
            if (!$favoriteActivity) {
                $favoriteActivity = new FavoriteActivity();
                $favoriteActivity->user_id = $user->id;
                $favoriteActivity->activity_id = $activityId;
                $favoriteActivity->save();
            }
            $response = array(
                'status' => true
            );

            return response()->json($response);
        }

        $response = array(
            'status' => false
        );
        return response()->json($response);
    }

    public function removeFavoriteActivity($activityId)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $favoriteActivity = FavoriteActivity::where('user_id', '=', $user->id)
                ->where('activity_id', '=', $activityId)->first();
            if ($favoriteActivity) {
                $favoriteActivity->delete();
            }

            $response = array(
                'status' => true
            );
            return response()->json($response);
        }

        $response = array(
            'status' => false
        );
        return response()->json($response);
    }

    public function getFavoriteActivities()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $favoriteActivities = FavoriteActivity::where('user_id', '=', $user->id)->get();
            $favoriteActivityIds = null;
            if ($favoriteActivities) {
                foreach ($favoriteActivities as $favoriteActivity) {
                    $favoriteActivityIds[] = $favoriteActivity->activity_id;
                }

                $activities = Activity::whereIn('id', $favoriteActivityIds)->get();
                return response()->json($activities);
            }
        }

        return response()->json(array());
    }

    public function isFavoriteActivity($activityId)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $favoriteActivity = FavoriteActivity::where('user_id', '=', $user->id)
                ->where('activity_id', '=', $activityId)->first();
            if ($favoriteActivity) {
                $response = array(
                    'status' => true,
                    'is_favorite' => true
                );
                return response()->json($response);
            } else {
                $response = array(
                    'status' => true,
                    'is_favorite' => false
                );
                return response()->json($response);
            }
        }
        $response = array(
            'status' => false
        );
        return response()->json($response);
    }
}
