<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\CreateGroupRequest;
use App\Http\Requests\UpdateGroupRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Admin;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function getRegister()
    {
        $auth = auth()->guard('admin');

        if($auth->check()) {
            return redirect('/admin/home');
        }
        return view('admin.register');
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:admins',
            'password' => 'required|confirmed|min:6',
        ]);

        $auth = auth()->guard('admin');

        if($auth->check()) {
            return redirect('/admin/home');
        }


        $admin = new Admin();
        $admin->name = $request['name'];
        $admin->email = $request['email'];
        $admin->password = bcrypt($request['password']);
        $admin->save();

        $credentials = [
            'email' =>  $request['email'],
            'password' =>  $request['password'],
        ];

        if ($auth->attempt($credentials)) {
            return redirect('/admin/home');
        }

        return abort(401);
    }

    public function getLogin()
    {
        $auth = auth()->guard('admin');

        if($auth->check()) {
            return redirect('/admin/home');
        }
        return view('admin.login');
    }

    public function postLogin(Request $request)
    {
        $auth = auth()->guard('admin');
        $credentials = [
            'email' =>  $request['email'],
            'password' =>  $request['password'],
        ];

        if ($auth->attempt($credentials)) {
            return redirect('/admin/home');
        }

        return abort(401);
    }

    public function getLogout()
    {
        $auth = auth()->guard('admin');
        $auth->logout();
        return redirect('/');
    }

    public function getHome()
    {
        $auth = auth()->guard('admin');
        if($auth->check()) {
            return view('admin.home');
        }
        return redirect('/admin/login');
    }

    public function getClubs()
    {
        $auth = auth()->guard('admin');
        if($auth->check()) {
            $groups = Group::orderBy('created_at', 'DESC')->get();
            return response()->json($groups);
        }

        return response()->json(array());
    }

    public function storeClub(CreateGroupRequest $request)
    {
        $auth = auth()->guard('admin');
        if($auth->check()) {
            $group = new Group();
            $group->name = $request['name'];
            $group->email = $request['email'];
            $group->password = bcrypt($request['password']);
            $group->type = $request['type'];
            $group->tel = $request['tel'];
            $group->address = $request['address'];
            $group->save();
            $group->status = true;

            return response()->json($group);
        }

        return response()->json(array());
    }

    public function updateClub(UpdateGroupRequest $request, $cid)
    {
        $auth = auth()->guard('admin');
        if($auth->check()) {
            $group = Group::where('id', '=', $cid)->first();
            $group->name = $request['name'];
            $group->email = $request['email'];
            if($request['password'] != "") {
                $group->password = bcrypt($request['password']);
            }
            $group->type = $request['type'];
            $group->tel = $request['tel'];
            $group->address = $request['address'];
            $group->save();
            $group->status = true;

            return response()->json($group);
        }

        return response()->json(array());
    }

    public function deleteClub($cid)
    {
        $auth = auth()->guard('admin');
        if($auth->check()) {
            $group = Group::find($cid);
            $response = [
                'status' => false
            ];
            if($group) {
                $group->delete();
                $response['status'] = true;
            }
            return response()->json($response);
        }

        return response()->json(array());
    }
}
