<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => 'web'], function () {
    /*
     * Global Routes
     */
    Route::get('/', 'WelcomeController@index');

    Route::get('/login', function () {
        return view('login');
    });

    Route::get('/register', function () {
        return view('register');
    });

    /*
     * User Routes
     */
    Route::get('/home', function () {
        return redirect('/my/ticket');
    });

    Route::controllers([
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController',
    ]);

    /*
     * Group Routes
     * /group/register and /group/login will change to GroupControllerAction after group auth middleware is created.
     */
    Route::get('/group/dashboard/activity/{activityId}/statistics', 'GroupController@dashboardHistogramData');
    Route::get('/group/dashboard/activity/{activityId}/amount', 'GroupController@dashboardAmountPieData');
    Route::get('/group/dashboard/activity/{activityId}/profit', 'GroupController@dashboardMoneyPieData');
    Route::get('/group/dashboard/activity/{activityId}', 'GroupController@dashboard');
    Route::get('/group/dashboard/report/{activityId}/show', 'GroupController@dashboardShowReport');
    Route::get('/group/dashboard/report/{activityId}', 'GroupController@dashboard');
    Route::get('/group/register', 'GroupController@getRegister');
    Route::get('/group/login', 'GroupController@getLogin');
    Route::get('/group/home', 'GroupController@home');
    Route::post('/group/register', 'GroupController@postRegister');
    Route::post('/group/login', 'GroupController@postLogin');
    Route::get('/group/logout', 'GroupController@logout');

    /*
     * Store Routes
     */
    Route::get('/stores/belongsto/group/{gid}', 'StoreController@showByGroup');
    Route::post('/store', 'StoreController@store');
    Route::put('/store/{store}', 'StoreController@update');

    /*
     * Activity Routes
     */
    Route::get('/activity', 'ActivityController@index');
    Route::get('/activity/all', function () {
        return redirect('/activity/all/page/1');
    });
    Route::put('/activity/preview', 'ActivityController@preview');
    Route::get('/activity/all/page/{page}', 'ActivityController@all');
    Route::get('/activity/search/{keyword}/type/{type}/page/{page}', 'ActivityController@search');
    Route::get('/activity/create', 'ActivityController@create');
    Route::post('/activity/store', 'ActivityController@store');
    Route::get('/activity/{aid}/edit', 'ActivityController@edit');
    Route::put('/activity/{aid}', 'ActivityController@update');
    Route::get('/activity/{aid}', 'ActivityController@show');

    /*
     * Ticket API
     */
    Route::get('/my/ticket', 'UserHomeController@index');
    Route::post('/ticket/store', 'TicketController@store');
    Route::get('/ticket/gift', 'TicketController@receivedGift');
    Route::post('/ticket/gift', 'TicketController@sendGift');
    Route::get('/api/ticket/{qrcodeid}/qrcode/token/{token}', 'TicketController@qrcodeImage');

    /*
     * Upload Gift Photo
     */
    Route::post('/photo/upload', 'ActivityController@photoUpload');

    /*
     * Search User
     */
    Route::get('/api/search/user/{query}', function ($query) {
        if(auth()->check()) {
            $users = \App\User::where('email', 'like', '%' . $query . '%')
                ->orWhere('name', 'like', '%' . $query . '%')
                ->groupBy('id')
                ->get();

            $resultItems = array();
            $i = 1;
            foreach ($users as $user) {
                $resultItem['description'] = $user->name;
                $resultItem['title'] = $user->email;
                $resultItems[] = $resultItem;
                $i++;
            }

            $response = array(
                'results' => $resultItems
            );

            return response()->json($response);
        }

        return response()->json(array());
    });

    /*
     * Search Activity Page Auto Complete Routes
     */
    Route::get('/api/search/{keyword}', 'ApiController@search');

    /*
     * Super User APIs
     */
    Route::delete('/admin/club/delete/{cid}', 'AdminController@deleteClub');
    Route::put('/admin/club/update/{cid}', 'AdminController@updateClub');
    Route::post('/admin/club/store', 'AdminController@storeClub');
    Route::controller('/admin', 'AdminController');

    /*
     * Android Client Routes
     */
    Route::get('/api/activity/tickets/{activityId}', 'ApiController@getTicketsOfActivity');
    Route::get('/api/activity/stores/{activityId}', 'ApiController@getStoresOfActivity');
    Route::post('/api/user/login', 'ApiController@userLogin');
    Route::get('/api/user/logout', 'ApiController@userLogout');
    Route::get('/api/ticket/', 'TicketController@getTickets');
    Route::get('/api/ticket/activity/list', 'TicketController@getTicketActivityList');
    Route::post('/api/add/favorite/activity/{activityId}', 'ApiController@addFavoriteActivity');
    Route::delete('/api/remove/favorite/activity/{activityId}', 'ApiController@removeFavoriteActivity');
    Route::get('/api/favorite/activity', 'ApiController@getFavoriteActivities');
    Route::get('/api/is/favorite/activity/{activityId}', 'ApiController@isFavoriteActivity');
    /*
     * Android Store Routes
     */
    Route::post('/api/store/login', 'ApiController@storeLogin');
    Route::get('/api/store/logout', 'ApiController@storeLogout');
    Route::get('/api/store/activity', 'ApiController@storeActivityAll');
    Route::get('/api/publickey', 'TicketController@getPublicKey');
    Route::put('/api/disable/ticket', 'TicketController@disableTicket');
    Route::put('/api/bulk/disable/ticket', 'TicketController@bulkDisableTicket');
    Route::post('/api/store/sell/ticket', 'TicketController@sellTicketFromStore');
    /*
     * Mobile or Tablet common APIs
     */
    Route::get('/api/mobile/activity/{page}', 'ApiController@mobileIndex');
    Route::get('/api/search/activity/{keyword}', 'ApiController@searchActivity');


    Route::get('/group/activity/{aid}/ticket/report', 'GroupController@lookupTicketReport');
    Route::get('/group/download/activity/{aid}/ticket/report', 'GroupController@downloadTicketReport');
});
