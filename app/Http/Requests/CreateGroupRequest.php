<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateGroupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //anyone can make this reqeust
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:groups',
            'email' => 'required|email|unique:groups',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'type' => 'required',
            'tel' => 'required',
            'address' => 'required'
        ];
    }
}
