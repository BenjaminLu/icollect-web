<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Session;

class NewActivityRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return session('group_check') == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'image' => 'image|max:10240',
            'body' => 'required',
            'gain_start' => 'required',
            'gain_end' => 'required',
            'ex_start' => 'required',
            'ex_end' => 'required'
        ];
    }

}
