<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Session;

class NewStoreRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return session('group_check') == true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'tel' => 'required',
            'password' => 'required|min:6',
            'has_network' => 'required'
        ];
    }

}
