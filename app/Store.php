<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = ['name', 'password', 'address', 'tel', 'has_network'];

    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function setHasNetworkAttribute($boolean)
    {
        $tinyInt = ($boolean == 'true') ? 1 : 0;
        $this->attributes['has_network'] = $tinyInt;
    }

    public function getHasNetworkAttribute($tinyInt)
    {
        $boolean = ($tinyInt == 1) ? true : false;
        return $boolean;
    }
}
