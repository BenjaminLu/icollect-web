<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteActivity extends Model
{
    protected $table = 'favorite_activities';
    public $timestamps = false;
}
