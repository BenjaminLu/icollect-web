var app = require('express')();
var fs = require('fs');
var options = {
    pfx: fs.readFileSync('/opt/pfx/server.pfx'),
    passphrase:'novell'
};

var https = require('https').createServer(options, app);
var io = require('socket.io')(https);
var Redis = require('ioredis');
var redis = new Redis({
    host: '192.168.1.2',
    port: 6379
});
var pageViewSetter = new Redis({
    host: '192.168.1.2',
    port: 6379
});

var activityStatistics = [];
var socketIdActivityIdMap = [];
io.on('connection', function(socket) {
    socket.on('disconnect', function() {
        var activityId = socketIdActivityIdMap[socket.id];
        if(activityId) {
            activityStatistics[activityId]--;
            io.emit('user.leave.report', JSON.stringify({activityId : activityId}));
        }
    });

    socket.on('lookup.realtime.user', function(activityId) {
        var counter = activityStatistics[activityId];
        if(!counter) {
            counter = 0;
        }
        io.emit('realtime.user.report', JSON.stringify({activityId:activityId, count:counter}));
    });

    socket.on('mobile.user.leave.activity', function(activityId) {
        activityStatistics[activityId]--;
        io.emit('user.leave.report', JSON.stringify({activityId : activityId}));
    });

    socket.on('mobile.user.watch.activity', function(activityId) {
        if(!activityStatistics[activityId]) {
            activityStatistics[activityId] = 0;
        }

        io.emit('user.add.report', JSON.stringify({activityId : activityId}));
        activityStatistics[activityId]++;
        pageViewSetter.get('activity.pageview.' + activityId, function(err, pageViewCount) {
            if(!pageViewCount) {
                pageViewCount = 0;
            }
            pageViewCount++;
            pageViewSetter.set('activity.pageview.' + activityId, pageViewCount);
        });
    });

    socket.on('user.watch.activity', function(activityId) {
        if(!activityStatistics[activityId]) {
            activityStatistics[activityId] = 0;
        }

        io.emit('user.add.report', JSON.stringify({activityId : activityId}));

        socketIdActivityIdMap[socket.id] = activityId;
        activityStatistics[activityId]++;

        pageViewSetter.get('activity.pageview.' + activityId, function(err, pageViewCount) {
            if(!pageViewCount) {
                pageViewCount = 0;
            }
            pageViewCount++;
            pageViewSetter.set('activity.pageview.' + activityId, pageViewCount);
        });
    });
});

redis.subscribe('activity-published', function(err, count) {

});

redis.subscribe('received-gift', function(err, count) {

});

redis.on('message', function (channel, message) {
    if(channel == "activity-published") {
        io.emit('new-activity-published', message);
    } else if(channel == "received-gift") {
        var receiveGiftInfo = JSON.parse(message);
        var receiverId = receiveGiftInfo.receiver_id;
        io.emit('received-gift-' + receiverId, message);
    }
});

https.listen(3000, function() {
});
