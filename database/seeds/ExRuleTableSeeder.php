<?php

use App\ExRule;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExRuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$ex_Rules = null;
    	for($i = 1; $i <= 43; $i++) {
	        $ex_Rule = new stdClass();
	        $ex_Rule->name = '入場卷';
	        $ex_Rule->body = '入場卷';
	        $ex_Rule->quantity = 100;
	        $ex_Rule->activity_id = $i;
            $ex_Rule->image = 'image/gift.jpg';
	        $ex_Rules[] = $ex_Rule;
    	}

        DB::table('ex_rules')->truncate();

        foreach($ex_Rules as $act) {
            ExRule::create(array(
                'name' => $act->name,
                'body' => $act->body,
                'quantity' => $act->quantity,
                'activity_id' => $act->activity_id, 
                'image' => $act->image
            ));
        }
    }
}
