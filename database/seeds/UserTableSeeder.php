<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        User::create(array(
                'name' => "呂峻豪",
                'email' => 'coopermilk123@gmail.com',
                'password' => bcrypt('howard123')
            ));
    }
}
