<?php

use App\Group;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            '自然生態保育社',
            '攝影研究社',
            '新聞社',
            '創新事業研習社',
            '現代管理研習社',
            '資訊科學研習社',
            '性別文化研究社',
            '機車研究社',
            '智海學社',
            '波希米亞西畫社',
            '動畫漫畫社',
            '臨池書法社',
            '花藝社',
            '禪悅社',
            '蝴蝶蘭禮儀大使',
            '自由車社',
            '溜冰社',
            '花式調飲社',
            '口琴社',
            '長虹吉他社',
            '劇坊',
            '舞楓手語社',
            '精品咖啡研習社',
            '學生會'
        ];

        DB::table('groups')->truncate();

        foreach($names as $key => $name) {
            $i = $key + 1;
            Group::create(array(
                'name' => $name,
                'email' => 'nchu00' . $i . '@nchu.edu.tw',
                'password' => bcrypt('123456'),
                'type' => '其他',
                'address' => '中興大學',
                'tel' => '12345789',
            ));
        }
    }
}
