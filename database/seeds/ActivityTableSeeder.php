<?php

use App\Activity;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activities = null;
        $now = Carbon::now();
        $aWeekAfter = Carbon::now()->addDays(7);
        $twoWeekAfter = Carbon::now()->addDays(14);

        $activity = new Activity();
        $activity->name = '晨間植辦';
        $activity->image = 'image/1/1.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 1;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '撥雲見日成果展';
        $activity->image = 'image/2/2.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 2;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '興閱坊 蝶舞';
        $activity->image = 'image/3/3.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 3;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '創業社課';
        $activity->image = 'image/4/4.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 4;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '人際互動與溝通--能量生命密碼';
        $activity->image = 'image/5/5.png';
        $activity->location = '理學院大樓';
        $activity->group_id = 5;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '開工計畫';
        $activity->image = 'image/6/6.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 6;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '跳躍性別光譜的精靈：談酷兒影展(可認證本校通識自主學習或公務人員終身學習時數)';
        $activity->image = 'image/7/7.png';
        $activity->location = '理學院大樓';
        $activity->group_id = 7;
        $activity->gain_start = $now;
        $activity->gain_end = $aWeekAfter;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '冬至湯圓暨歐趴糖大會';
        $activity->image = 'image/9/8.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 9;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '迎新茶會';
        $activity->image = 'image/10/9.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 10;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '第五次繪圖社課';
        $activity->image = 'image/11/10.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 11;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '大型花束圓款';
        $activity->image = 'image/13/11.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 13;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '小圓垂掛揷花';
        $activity->image = 'image/13/12.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 13;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '蜘蛛抱蛋揷花';
        $activity->image = 'image/13/13.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 13;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = 'All pass糖';
        $activity->image = 'image/13/14.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 13;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '創造未來，操之在己(To Seize Future Ourselves)';
        $activity->image = 'image/14/15.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 14;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '你不理財，就被財理 Wealth, carry or lose?';
        $activity->image = 'image/14/16.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 14;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '樂活人生 LOHAS';
        $activity->image = 'image/14/17.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 14;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '勇渡煩惱河(一) Cross the worries';
        $activity->image = 'image/14/18.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 14;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '勇渡煩惱河(二) Cross the worries II';
        $activity->image = 'image/14/19.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 14;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '第一堂社課 ── 團員大會';
        $activity->image = 'image/15/20.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 15;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '儀態大社課';
        $activity->image = 'image/15/21.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 15;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '煞車-剎車的種類、如何維修及調整';
        $activity->image = 'image/16/22.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 16;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '鍊條保養、拆裝全教學';
        $activity->image = 'image/16/23.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 16;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '單車保養、障礙排除一日精華班';
        $activity->image = 'image/16/24.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 16;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '星鑽×自由古巴';
        $activity->image = 'image/18/25.png';
        $activity->location = '理學院大樓';
        $activity->group_id = 18;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '11/18花式社課';
        $activity->image = 'image/18/26.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 18;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '花式調飲社-經典社課';
        $activity->image = 'image/18/27.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 18;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '吉末轟炸';
        $activity->image = 'image/20/28.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 20;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '表演課♥ 小中老師進階表演班';
        $activity->image = 'image/21/29.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 21;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '表演課♥ 大弋老師表演基礎班';
        $activity->image = 'image/21/30.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 21;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '表演課♥ 小中老師表演進階班';
        $activity->image = 'image/21/31.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 21;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '興大劇坊2015新生公演《最後的遺言》';
        $activity->image = 'image/21/32.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 21;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '【週三社聚】 形容篇';
        $activity->image = 'image/22/33.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 22;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '【社課】情緒篇';
        $activity->image = 'image/22/34.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 22;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '【週三社聚】情緒篇進階';
        $activity->image = 'image/22/35.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 22;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '【社課】自然篇';
        $activity->image = 'image/22/36.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 22;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '咖啡『品產&器材』課';
        $activity->image = 'image/23/37.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 23;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '咖啡泡沫，靜靜生活';
        $activity->image = 'image/23/38.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 23;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '《破．渣》';
        $activity->image = 'image/23/39.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 23;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '幫喬啡．Bon cafe';
        $activity->image = 'image/23/40.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 23;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '和朋友們坐在草地上看電影';
        $activity->image = 'image/24/41.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 24;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = '中興大學演唱會';
        $activity->image = 'image/24/42.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 24;
        $activities[] = $activity;

        $activity = new Activity();
        $activity->name = 'Art與Tech的邂逅ing ─ 當傳統藝術遇見尖端科技';
        $activity->image = 'image/24/43.jpg';
        $activity->location = '理學院大樓';
        $activity->group_id = 24;
        $activities[] = $activity;

        DB::table('activities')->truncate();

        foreach($activities as $activity) {
            $activity->gain_start = $now;
            $activity->gain_end = $aWeekAfter;
            $activity->ex_start = $now;
            $activity->ex_end = $twoWeekAfter;
            $activity->save();
        }
    }
}
