<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExRulesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ex_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 20);
            $table->string('body', 50);
            $table->integer('quantity')->unsigned();
            $table->string('image', 100);
            $table->integer('activity_id')->unsigned();
            $table->timestamps();

            $table->foreign('activity_id')
                ->references('id')
                ->on('activities')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ex_rules');
    }

}
