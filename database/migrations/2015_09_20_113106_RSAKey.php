<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RSAKey extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rsa_keys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pair');
            $table->string('type', 20);
            $table->text('modulus');
            $table->text('exponent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rsa_keys');
    }

}
