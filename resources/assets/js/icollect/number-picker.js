/**
 * Created by Administrator on 2015/9/19.
 */
$(function() {
    var action;
    $(".number-spinner button").on('click', function () {
        btn = $(this);
        input = btn.closest('.number-spinner').find('input');
        btn.closest('.number-spinner').find('button').prop("disabled", false);

        if (btn.attr('data-dir') == 'up') {
            if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                input.val(parseInt(input.val())+1);
            }else{
                btn.prop("disabled", true);
                clearInterval(action);
            }
        } else {
            if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                input.val(parseInt(input.val())-1);
            }else{
                btn.prop("disabled", true);
                clearInterval(action);
            }
        }
    }).mouseup(function(){
        clearInterval(action);
    });
});