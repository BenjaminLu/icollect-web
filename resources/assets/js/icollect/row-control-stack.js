var RowControlStack = function(array) {
    var stack = [];
    var _this = this;
    var init = function(array) {
        if(typeof array != 'undefined') {
            array.forEach(function(entry) {
                stack.push(entry);
                stack.push(top);
            });
        }
    };

    _this.getNextIndex = function() {
        return stack.length;
    };

    _this.showStack = function() {
        for(var i = 0 ; i < stack.length; i++) {
            console.log(stack[i]);
        }
    };

    _this.push = function(item) {
        stack.push(item);
    };

    _this.remove = function(index) {
        if (index > -1) {
            stack.splice(index, 1);
        }
    };

    _this.updateIndexesOfGainSection = function(gainSection) {
        var newStack = [];
        for(var i = 0 ; i < stack.length ; i++) {
            newStack.push(i);
        }

        stack = newStack.slice();
        var idStack = newStack.slice();
        var minusBtnStack = newStack.slice();
        var bodyStack = newStack.slice();
        var valueStack = newStack.slice();

        $(gainSection).find('.gain-row-id').each(function(index) {
            var number = idStack.shift();
            console.log(number);
            $(this).attr('name', 'gain_rules[' + number + '][id]');
        });

        $(gainSection).find('.gain-minus-btn').each(function(index) {
            var number = minusBtnStack.shift();
            console.log(number);
            $(this).attr('data-index', number);
        });

        $(gainSection).find('.gain-row-body').each(function(index) {
            var number = bodyStack.shift();
            console.log(number);
            $(this).attr('name', 'gain_rules[' + number + '][body]').attr('data-index', number);
        });

        $(gainSection).find('.gain-row-value').each(function(index) {
            var number = valueStack.shift();
            console.log(number);
            $(this).attr('name', 'gain_rules[' + number + '][value]').attr('data-index', number);
        });
    };

    _this.updateIndexesOfExSection = function(exSection) {
        var newStack = [];
        for(var i = 0 ; i < stack.length ; i++) {
            newStack.push(i);
        }

        stack = newStack.slice();
        var idStack = newStack.slice();
        var minusBtnStack = newStack.slice();
        var exImageUrlStack = newStack.slice();
        var nameStack = newStack.slice();
        var bodyStack = newStack.slice();
        var thresholdStack = newStack.slice();

        $(exSection).find('.ex-row-id').each(function(index) {
            var number = idStack.shift();
            $(this).attr('name', 'ex_rules[' + number +'][id]');
        });

        $(exSection).find('.ex-minus-btn').each(function(index) {
            var number = minusBtnStack.shift();
            $(this).attr('data-index', number);
        });

        $(exSection).find('.ex-row-image-url').each(function(index) {
            var number = exImageUrlStack.shift();
            $(this).attr('name', 'ex_rules[' + number +'][image]');
        });

        $(exSection).find('.ex-row-name').each(function(index) {
            var number = nameStack.shift();
            $(this).attr('name', 'ex_rules[' + number +'][name]').attr('data-index', number);
        });

        $(exSection).find('.ex-row-body').each(function(index) {
            var number = bodyStack.shift();
            $(this).attr('name', 'ex_rules[' + number +'][body]').attr('data-index', number);
        });

        $(exSection).find('.ex-row-threshold').each(function(index) {
            var number = thresholdStack.shift();
            $(this).attr('name', 'ex_rules[' + number +'][threshold]').attr('data-index', number);
        });
    };
    init(array);
};
