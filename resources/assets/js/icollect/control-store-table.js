/**
 * Created by BenjaminLu on 2015/6/5.
 */
var StoreTableControl = function(controls) {
    var instance = this;
    var table = controls.table;
    var passwordControl = controls.passwordControl;
    var searchControl = controls.searchControl;
    var initURL = controls.initURL;
    var updateModal = controls.updateModal;
    var updateURL = controls.updateURL;
    var csrfToken = controls.csrfToken;
    var updateModalHeaderID = $('#update-store-id');
    $('.ui.checkbox').checkbox();

    $(window).on("load resize", function() {
        var isSmall = $(window).width() < 768;
        if(isSmall) {
            var value = $(searchControl).val();
            value = value.toLowerCase();
            instance.setSearchResult(value);
            $(passwordControl).parent().removeClass('toggle');
        } else {
            $(passwordControl).parent().addClass('toggle');
        }
    });

    this.setSearchResult = function(value) {
        if(value == "") {
            $('#store-table tr').removeAttr('style').show();
            $('#store-table .highlight').removeClass('highlight');
            return;
        }

        $('#store-table tbody tr').each(function(index) {
            var idTD = $(this).find('td:nth-child(1) span');
            var nameTD = $(this).find('td:nth-child(2) span');
            var addressTD = $(this).find('td:nth-child(3) span');
            var telTD = $(this).find('td:nth-child(4) span');

            var id = idTD.text().toLowerCase();
            var name = nameTD.text().toLowerCase();
            var address = addressTD.text().toLowerCase();
            var tel = telTD.text().toLowerCase();

            var idPos = id.indexOf(value);
            var namePos = name.indexOf(value);
            var addressPos = address.indexOf(value);
            var telPos = tel.indexOf(value);

            var isHit = (idPos != -1) || (namePos != -1) || (addressPos != -1) || (telPos != -1);

            if(idPos >= 0) {
                idTD.addClass('highlight');
            } else {
                idTD.removeClass('highlight');
            }

            if(namePos >= 0) {
                nameTD.addClass('highlight');
            } else {
                nameTD.removeClass('highlight');
            }

            if(addressPos >= 0) {
                addressTD.addClass('highlight');
            } else {
                addressTD.removeClass('highlight');
            }

            if(telPos >= 0) {
                telTD.addClass('highlight');
            } else {
                telTD.removeClass('highlight');
            }

            if(isHit) {
                $(this).removeAttr('style');
            } else {
                $(this).css('display','none');
            }
        });
    }
    searchControl.on("keyup", function() {
        var value = $(this).val();
        value = value.toLowerCase();
        instance.setSearchResult(value);
    });

    this.isStoreHasChanged = function(store_id, params) {
        var containsDataEditBtn = $('button[data-store-id=' + store_id +']').filter(function(index){
            return $(this).hasClass('edit-store');
        });

        var name = $(containsDataEditBtn).attr('data-store-name');
        var address = $(containsDataEditBtn).attr('data-store-address');
        var tel = $(containsDataEditBtn).attr('data-store-tel');
        var password = $(containsDataEditBtn).attr('data-store-password');
        var has_network = containsDataEditBtn.attr('data-store-has_network');

        if(has_network == 'true')
            has_network = true;
        else
            has_network = false;
        if(name != params['name'])
            return true;
        if(address != params['address'])
            return true;
        if(tel != params['tel'])
            return true;
        if(password != params['password'])
            return true;
        if(has_network != params['has_network'])
            return true;
        return false;
    }

    $('#submit-update-store').on('click', function() {
        var store_id = updateModalHeaderID.text();
        var data = {
            '_token' : csrfToken,
            'name' : updateModal.find('input[name=name]').val(),
            'address' : updateModal.find('input[name=address]').val(),
            'tel' : updateModal.find('input[name=tel]').val(),
            'password' : updateModal.find('input[name=password]').val(),
            'has_network' : updateModal.find('input[name=has_network]').is(':checked')
        };
        var hasChanged = instance.isStoreHasChanged(store_id, data);
        console.log(data);
        if(hasChanged == true) {
            $.ajax({
                url: updateURL + '/' + store_id,
                data: data,
                type:'PUT',
                dataType:'json',

                success: function(store) {
                    instance.updateTableRowData(store);
                },

                error:function(xhr, ajaxOptions, thrownError){
                    $('#error-modal').modal('show');
                }
            });
        }
    });

    $(document).on('click', '.edit-store', function() {
        var tr = $(this).parent('td').parent('tr');
        var store = instance.getStoreJSONFromBtn($(this));
        instance.setStoreInfoToUpdateModal(store);
        updateModal.modal('setting', 'transition', 'horizontal flip').modal('show');
    });

    $(document).on('click', '.delete-store', function() {
        alert('delete');
    });

    $.ajax({
        url : initURL,
        type : 'GET',
        success: function(stores) {
            for(var i = (stores.length - 1); i >= 0; i--) {
                instance.appendNewStoreToTable(stores[i]);
            }
        },

        error:function(xhr, ajaxOptions, thrownError) {
            alert('獲取商家資料失敗');
        }
    });

    passwordControl.on('change', function() {
        var checked = this.checked;
        if(checked) {
            $('.password').addClass('password-show').removeClass('password');
        } else {
            $('.password-show').addClass('password').removeClass('password-show');
        }
    });

    this.getStoreJSONFromBtn = function(btn) {
        var store = {};
        store.id = btn.attr('data-store-id');
        store.name =  btn.attr('data-store-name');
        store.address =  btn.attr('data-store-address');
        store.tel = btn.attr('data-store-tel');
        store.password =  btn.attr('data-store-password');

        if(btn.attr('data-store-has_network') == 'true')
            store.has_network = true
        else
            store.has_network = false;
        return store;
    }

    this.makeAddressTD = function(store)
    {
        var address = $('<a>').attr({
            'href' : 'https://www.google.com.tw/maps/place/' + store.address,
            'target' : '_blank'
        }).append(store.address);
        return address;
    }

    this.makeCheckboxTD = function(store) {
        var checked = store.has_network;
        var checkbox = $('<div>').addClass('ui checkbox').append('<input type="checkbox">', '<label>有</label>');
        if(checked) {
            checkbox.checkbox('check');
        } else {
            checkbox.checkbox('uncheck');
        }
        checkbox.addClass('read-only');
        return checkbox;
    }

    this.updateTableRowData = function(store) {
        var tr = table.find('tr[data-store-id=' + store.id +']');
        $(tr).empty();
        var address = this.makeAddressTD(store);
        var checkbox = this.makeCheckboxTD(store);

        tr.append($('<td>').append($('<span>').append(store.id)));
        tr.append($('<td>').append($('<span>').append(store.name)));
        tr.append($('<td>').append($('<span>').append(address)));
        tr.append($('<td>').append($('<span>').append(store.tel)));
        if(passwordControl.is(':checked')) {
            tr.append($('<td>').addClass('password-show').append(store.password));
        } else {
            tr.append($('<td>').addClass('password').append(store.password));
        }

        tr.append($('<td>').append(checkbox));
        tr.append(
            $('<td>').append(
                $('<button>').attr('type','button').attr({
                    'data-store-id': store.id,
                    'data-store-name' : store.name,
                    'data-store-address' : store.address,
                    'data-store-tel' : store.tel,
                    'data-store-password' : store.password,
                    'data-store-has_network' : store.has_network
                }).addClass('btn btn-info edit-store').text('編輯')).append(
                $('<button>').attr('type','button').attr({
                    'data-store-id': store.id,
                    'data-store-name' : store.name,
                    'data-store-address' : store.address,
                    'data-store-tel' : store.tel,
                    'data-store-password' : store.password,
                    'data-store-has_network' : store.has_network
                }).addClass('btn btn-warning delete-store').text('刪除'))
        );
    }

    this.setStoreInfoToUpdateModal = function(store) {
        updateModalHeaderID.text(store.id);
        var updateStoreFormBody = $('#update-store-form');
        updateStoreFormBody.find('input[name=name]').val(store.name);
        updateStoreFormBody.find('input[name=address]').val(store.address);
        updateStoreFormBody.find('input[name=tel]').val(store.tel);
        updateStoreFormBody.find('input[name=password]').val(store.password);
        var updateModalCheckbox = updateStoreFormBody.find('.ui.checkbox');
        if(store.has_network == true) {
            $(updateModalCheckbox).checkbox('check');
        } else {
            $(updateModalCheckbox).checkbox('uncheck');
        }
    }

    this.appendNewStoreToTable = function(store) {
        var address = this.makeAddressTD(store);
        var checkbox = this.makeCheckboxTD(store);

        var row = $('<tr>').attr('data-store-id', store.id);
        row.append($('<td>').append($('<span>').append(store.id)));
        row.append($('<td>').append($('<span>').append(store.name)));
        row.append($('<td>').append($('<span>').append(address)));
        row.append($('<td>').append($('<span>').append(store.tel)));
        if(passwordControl.is(':checked')) {
            row.append($('<td>').addClass('password-show').append(store.password));
        } else {
            row.append($('<td>').addClass('password').append(store.password));
        }
        row.append($('<td>').append(checkbox));
        row.append(
            $('<td>').append(
                $('<button>').attr('type','button').attr({
                        'data-store-id': store.id,
                        'data-store-name' : store.name,
                        'data-store-address' : store.address,
                        'data-store-tel' : store.tel,
                        'data-store-password' : store.password,
                        'data-store-has_network' : store.has_network
                    }).addClass('btn btn-info edit-store').text('編輯')).append(
                $('<button>').attr('type','button').attr({
                        'data-store-id': store.id,
                        'data-store-name' : store.name,
                        'data-store-address' : store.address,
                        'data-store-tel' : store.tel,
                        'data-store-password' : store.password,
                        'data-store-has_network' : store.has_network
                    }).addClass('btn btn-warning delete-store').text('刪除'))
            );
        $(table).find('tbody').prepend(row);
    }
}