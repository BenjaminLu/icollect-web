import React from 'react';

export default class LeftMiles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    	count: 100
    };
  }

  render() {
    return (
      <h1 className='counter-red'>LeftMiles : {this.state.count}</h1>
    );
  }
}
