import React from 'react';
import LeftMiles from './LeftMiles.js'

class Counter extends React.Component {
    constructor(props) {
        super(props);
        let init = this.props.init;
        let max = this.props.max;
        let min = this.props.min;
        this.state = {
            count: init,
            max: max,
            min: min
        };
    }

    componentDidMount() {
        this._leftMiles.setState({count : this.state.max - this.state.count});
    }

    sub() {
        var count = this.state.count - 1;
        var leftMileCount = this._leftMiles.state.count + 1;
        if (count <= this.state.min) {
            count = this.state.min;
            leftMileCount = this.state.max;
        }
        this.setState({count : count});
        this._leftMiles.setState({count : leftMileCount});
    }

    add() {
        var count = this.state.count + 1;
        var leftMileCount = this._leftMiles.state.count - 1;
        if (count >= this.state.max) {
            count = this.state.max;
            leftMileCount = this.state.min;
        }
        this.setState({count : count});
        this._leftMiles.setState({count : leftMileCount});

    }

    headingGenerator() {
        if(this.props.hasHeader) {
            return <h1>Hello I'm heading</h1>
        }
    }

    render() {
        return (
            <div>
                {this.headingGenerator()}
                <h1 className="counter-green">Count : {this.state.count}</h1>
                <LeftMiles ref={(c) => this._leftMiles = c}/>
                <button onClick={this.sub.bind(this)}>-</button>
                <button onClick={this.add.bind(this)}>+</button>
            </div>
        );
    }
}

export default Counter