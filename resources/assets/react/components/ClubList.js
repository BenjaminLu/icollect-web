import React from 'react';
export default class ClubList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clubs: this.props.clubs,
            cid: 0, 
            index: 0
        };
    }

    componentDidMount() {
        this.props.componentDidMount();
    }

    onAddButtonClick() {
        this.props.onAddButtonClick();
    }

    onEditButtonClick(club, index) {
        this.setState({cid: club.id, index: index});
        this.props.onEditButtonClick(club);
    }

    onDeleteButtonClick(club, index) {
        this.setState({cid: club.id, index: index});
        this.props.onDeleteButtonClick(club);
    }

    onAddClubSubmit() {
        this.props.onAddClubSubmit("#new-club-form");
    }

    insertRow(newClub) {
        this.state.clubs.unshift(newClub);
        this.setState({clubs: this.state.clubs});
    }

    onEditClubSubmit() {
        this.props.onEditClubSubmit("#edit-club-form", this.state.cid);
    }

    updateRow(editClub) {
        this.state.clubs[this.state.index] = editClub;
        this.setState({clubs: this.state.clubs});
    }

    onDeleteClubSubmit() {
        this.props.onDeleteClubSubmit(this.state.cid);
    }

    deleteRow(editClub) {
        this.state.clubs.splice(this.state.index, 1);
        this.setState({clubs: this.state.clubs});
    }

    render() {
        return (
            <div>
                <section>
                    <a className="margin-bottom ui left floated small primary labeled icon button center" onClick={() => this.onAddButtonClick()}>
                        <i className="plus icon"></i> 新增社團
                    </a>
                </section>
                <table className="ui very compact definition celled table table-hover margin-top">
                    <thead className="full-width">
                    <tr>
                        <th className="center aligned">ID</th>
                        <th className="center aligned">名稱</th>
                        <th className="center aligned">email</th>
                        <th className="center aligned">類型</th>
                        <th className="center aligned">地址</th>
                        <th className="center aligned">電話</th>
                        <th className="center aligned">資料建於</th>
                        <th className="center aligned">最後更新</th>
                        <th className="center aligned">控制</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.clubs.map(function (club, index) {
                            return (
                                <tr key={"club-" + index}>
                                    <td><span>{club.id}</span></td>
                                    <td><span>{club.name}</span></td>
                                    <td><span>{club.email}</span></td>
                                    <td><span>{club.type}</span></td>
                                    <td><span>{club.address}</span></td>
                                    <td><span>{club.tel}</span></td>
                                    <td><span>{club.created_at}</span></td>
                                    <td><span>{club.updated_at}</span></td>
                                    <td className="center aligned">
                                        <div className="ui buttons">
                                            <button className="ui positive button"
                                                    onClick={() => this.onEditButtonClick(club, index)}>編輯
                                            </button>
                                            <div className="or"></div>
                                            <button className="ui negative button"
                                                    onClick={() => this.onDeleteButtonClick(club, index)}>刪除
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            );
                        }, this)
                    }
                    </tbody>
                </table>
                <div id="add-club-modal" className="ui modal">
                    <div className="header">
                        <i className="edit icon">

                        </i>
                        新增社團
                       <span id="update-store-id">

                       </span>
                    </div>
                    <div className="content">
                        <div className="row">
                            <form id="new-club-form" className="form-horizontal" role="form" method="POST"
                                  action="">
                                <div className="form-group">
                                    <label className="col-md-4 control-label">社團名稱</label>

                                    <div className="col-md-6">
                                        <input type="text" className="form-control" name="name"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">E-Mail</label>

                                    <div className="col-md-6">
                                        <input type="email" className="form-control" name="email"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">密碼</label>

                                    <div className="col-md-6">
                                        <input type="password" className="form-control" name="password"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">確認密碼</label>

                                    <div className="col-md-6">
                                        <input type="password" className="form-control" name="password_confirmation"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">類型</label>

                                    <div className="col-md-6">
                                        <select className="ui search dropdown" name="type">
                                            <option>學術性</option>
                                            <option>學藝性</option>
                                            <option>服務性</option>
                                            <option>聯誼性</option>
                                            <option>體育性</option>
                                            <option>康樂性</option>
                                            <option>自治性</option>
                                            <option>綜合性</option>
                                            <option>其他</option>
                                        </select>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">電話</label>

                                    <div className="col-md-6">
                                        <input type="tel" className="form-control" name="tel"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">地址</label>

                                    <div className="col-md-6">
                                        <input type="text" className="form-control" name="address"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui button positive" onClick={() => this.onAddClubSubmit()}>
                            新增
                        </div>
                        <div className="ui button">
                            取消
                        </div>
                    </div>
                </div>

                <div id="edit-club-modal" className="ui modal">
                    <div className="header">
                        <i className="edit icon">

                        </i>
                        編輯社團
                       <span id="update-store-id">

                       </span>
                    </div>
                    <div className="content">
                        <div className="row">
                            <form id="edit-club-form" className="form-horizontal" role="form" method="POST"
                                  action="">
                                <div className="form-group">
                                    <label className="col-md-4 control-label">社團名稱</label>

                                    <div className="col-md-6">
                                        <input id="edit-club-name" type="text" className="form-control" name="name"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">E-Mail</label>

                                    <div className="col-md-6">
                                        <input id="edit-club-email" type="email" className="form-control" name="email"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">新密碼</label>

                                    <div className="col-md-6">
                                        <input id="edit-club-password" type="password" className="form-control"
                                               name="password"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">確認密碼</label>

                                    <div className="col-md-6">
                                        <input id="edit-club-password-confirm" type="password" className="form-control"
                                               name="password_confirmation"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">類型</label>

                                    <div className="col-md-6">
                                        <select id="edit-club-type" className="ui search dropdown" name="type">
                                            <option value="學術性">學術性</option>
                                            <option value="學藝性">學藝性</option>
                                            <option value="服務性">服務性</option>
                                            <option value="聯誼性">聯誼性</option>
                                            <option value="體育性">體育性</option>
                                            <option value="康樂性">康樂性</option>
                                            <option value="自治性">自治性</option>
                                            <option value="綜合性">綜合性</option>
                                            <option value="其他">其他</option>
                                        </select>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">電話</label>

                                    <div className="col-md-6">
                                        <input id="edit-club-tel" type="tel" className="form-control" name="tel"/>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="col-md-4 control-label">地址</label>

                                    <div className="col-md-6">
                                        <input id="edit-club-address" type="text" className="form-control"
                                               name="address"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="actions">
                        <div id="submit-update-store" className="ui button positive" onClick={() => this.onEditClubSubmit()}>
                            儲存
                        </div>
                        <div className="ui button">
                            取消
                        </div>
                    </div>
                </div>

                <div id="delete-club-modal" className="ui modal">
                    <div className="header">
                        <i className="edit icon">

                        </i>
                        刪除社團
                       <span id="update-store-id">

                       </span>
                    </div>
                    <div className="content">
                        <div className="row">
                            <h4>確定刪除<span id="delete-club-name" className="text-danger"></span>嗎？</h4>
                        </div>
                    </div>
                    <div className="actions">
                        <div id="submit-update-store" className="ui button negative" onClick={() => this.onDeleteClubSubmit()}>
                            刪除
                        </div>
                        <div className="ui button">
                            取消
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}