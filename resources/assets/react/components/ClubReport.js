import React from 'react';

export default class ClubReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 500
        };
    }

    render() {
        return (
            <h1 className='counter-red'>ClubReport : {this.state.count}</h1>
        );
    }
}
