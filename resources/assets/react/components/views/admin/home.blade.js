import React from 'react';
import ReactDOM from 'react-dom';
import ClubList from '../../ClubList.js';

var onAddButtonClick = function() {
    $('#add-club-modal').modal('show');
};

var clubList, editClubModal, editClubName, editClubEmail, editClubType, editClubTel, editClubAddress;

var onEditButtonClick = function(club) {
    editClubName.val(club.name);
    editClubEmail.val(club.email);
    editClubType.dropdown('set selected', club.type);
    editClubTel.val(club.tel);
    editClubAddress.val(club.address);
    editClubModal.modal('show');
};

var onDeleteButtonClick = function(club) {
    $('#delete-club-name').text(club.name);
    $('#delete-club-modal').modal('show');
};

var componentDidMount = function() {
    $('#club-list .dropdown').dropdown({
        transition: 'drop'
    });
    editClubModal = $('#edit-club-modal');
    editClubName = $('#edit-club-name');
    editClubEmail = $('#edit-club-email');
    editClubType = $('#edit-club-type');
    editClubTel = $('#edit-club-tel');
    editClubAddress = $('#edit-club-address');

    $('.ui.modal').modal({detachable: false});
};

var onAddClubSubmit = function(newClubFormId) {
    $.post('/admin/club/store', $(newClubFormId).serialize())
    .done(function(newClub) {
        if (newClub.status == true) {
            clubList.insertRow(newClub);
        } else {
            alert("新增失敗");
        }
    })
    .fail(function(xhr, textStatus, errorThrown) {
        alert(xhr.responseText);
    });
};

var onEditClubSubmit = function(editClubFormId, cid) {
    $.ajax({
        url: '/admin/club/update/'+cid,
        type: 'PUT',
        data: $(editClubFormId).serialize(),
        success: function(editClub) {
            if (editClub.status == true) {
                alert("修改成功");
                clubList.updateRow(editClub);
            } else {
                alert("修改失敗");
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            alert(xhr.responseText);
        }
    });
};

var onDeleteClubSubmit = function(cid) {
    $.ajax({
        url: '/admin/club/delete/'+cid,
        type: 'DELETE',
        success: function(deleteClub) {
            if (deleteClub.status == true) {
                alert("刪除成功");
                clubList.deleteRow(deleteClub);
            } else {
                alert("刪除失敗");
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            alert(xhr.responseText);
        }
    });
};

$.getJSON('/admin/clubs', function(clubs) {
    clubList = ReactDOM.render(<ClubList
        clubs={clubs}
        onAddButtonClick={onAddButtonClick}
        onEditButtonClick={onEditButtonClick}
        onDeleteButtonClick={onDeleteButtonClick}
        componentDidMount={componentDidMount}
        onAddClubSubmit={onAddClubSubmit}
        onEditClubSubmit={onEditClubSubmit}
        onDeleteClubSubmit={onDeleteClubSubmit}
    />, document.getElementById('club-list'));
});