@extends('app')

@section('content')
    <div id="register-section" class="container-fluid">
        <div class="row">
            <!-- user register -->
            <div id="user-register-section" class="col-md-8 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading"><h4></h4></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>管理者註冊</h2>
                            </div>
                        </div>
                        <hr/>
                        <form id="user-register-form" class="form-horizontal" role="form" method="POST"
                              action="{{ url('/admin/register') }}">
                            @if ( count($errors) > 0 )
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">姓名</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">E-Mail</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">密碼</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">確認密碼</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-1 col-md-offset-4">
                                    <button type="submit" class="btn btn-info">
                                        註冊
                                    </button>
                                </div>

                                <div class="col-md-1">
                                    <a href="{{url('/register')}}" class="btn btn-info rollback-btn">
                                        返回
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partial.footer')
@endsection
