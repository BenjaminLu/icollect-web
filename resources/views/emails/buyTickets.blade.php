{{$userName}}您買了{{$ticketNumber}}張票卷

<h4>{{$activity_name}}</h4>
<img src="{{$activity_image}}"/>

<div class="visible-print text-center">
    @if(count($qrcode_urls) > 0)
        @foreach($qrcode_urls as $qrcode_url)
            <img src="{{url($qrcode_url)}}"/>
        @endforeach
    @endif
</div>