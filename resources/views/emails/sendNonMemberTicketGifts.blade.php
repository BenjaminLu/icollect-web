活動 : {{$activity_name}}<br/>
<img src="{{$activity_image}}"/><br/>
<br/>
{{$senderName}}送給了您{{$ticketNumber}}張票卷，以及想對您說 :<br/>
<br/>
{{$giftMessage}}<br/>
<br/>

票卷列表 :<br/>
<br/>
<div class="visible-print text-center">
    @if(count($qrcodeUrls) > 0)
        @foreach($qrcodeUrls as $qrcodeUrl)
            <img src="{{url($qrcodeUrl)}}"/>
        @endforeach
    @endif
</div>