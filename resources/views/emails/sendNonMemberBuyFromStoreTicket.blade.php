活動 : {{$activity_name}}<br/>
<img src="{{$activity_image}}"/><br/>
<br/>
您購買了{{$ticketNumber}}張票卷<br/>
<br/>
票卷列表 :<br/>
<br/>
<div class="visible-print text-center">
    @if(count($qrcodeUrls) > 0)
        @foreach($qrcodeUrls as $qrcodeUrl)
            <img src="{{url($qrcodeUrl)}}"/>
        @endforeach
    @endif
</div>