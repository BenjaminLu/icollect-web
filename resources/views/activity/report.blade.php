@extends('app')

@section('content')
    <section class="container">
        <section class="hint">
            <div class="ui ordered steps center steps-hint">
                <div class="active step">
                    <div class="content">
                        <div class="title">新增活動進場地點</div>
                        <div class="description">增加進場地點，權限控管！</div>
                    </div>
                </div>
                <div class="active step">
                    <div class="content">
                        <div class="title">活動</div>
                        <div class="description">發佈活動，推播行銷！</div>
                    </div>
                </div>
                <div class="active step">
                    <div class="content">
                        <div class="title">經營</div>
                        <div class="description">運作社團App！</div>
                    </div>
                </div>
                <div class="completed step">
                    <div class="content">
                        <div class="title">輔助經營</div>
                        <div class="description">踴躍度報表、學生回饋！</div>
                    </div>
                </div>
            </div>
        </section>
        <div class="row clearfix">
            <h2 class="pull-left ui header pull-left">
                <div class="content col-md-12">
                    {{$activity->name}}
                    <div class="sub header">參與報表</div>
                </div>
            </h2>

            <a class="pull-right margin-bottom ui left floated small primary labeled icon button center"
               target="_blank"
               href="{{url('/group/download/activity/' . $activity->id . '/ticket/report')}}">
                <i class="download icon"></i>下載參與報表
            </a>
        </div>
        <div class="margin-bottom">
            <table id="activity-table" class="ui striped table-hover table">
                <thead class="full-width">
                <tr>
                    <th>票券編號</th>
                    <th>票券名稱</th>
                    <th>票券內容</th>
                    <th>票券使用情形</th>
                    <th>取票人</th>
                    <th>取票人Email</th>
                    <th>取票時間</th>
                </tr>
                </thead>
                <tbody>
                @if($reports)
                @foreach($reports as $report)
                    <tr>
                        <td>
                            <span>{{$report->ticketId}}</span>
                        </td>
                        <td>
                            <span>{{$report->ticketName}}</span>
                        </td>
                        <td>
                            <span>{{$report->ticketBody}}</span>
                        </td>
                        <td>
                            <span>{{$report->isUsed}}</span>
                        </td>
                        <td>
                            <span>{{$report->taker}}</span>
                        </td>
                        <td>
                            <span>{{$report->takerEmail}}</span>
                        </td>
                        <td>
                            <span>{{$report->takeTime}}</span>
                        </td>
                    </tr>
                @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </section>
@endsection