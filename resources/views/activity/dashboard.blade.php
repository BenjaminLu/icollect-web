@extends('app')
@section('title', ' - '. '活動儀表板')
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('css')
    <style>

        .bar rect {
            fill: steelblue;
            shape-rendering: crispEdges;
        }

        .bar text {
            fill: #fff;
        }

        .axis path, .axis line {
            fill: none;
            stroke: #000;
            shape-rendering: crispEdges;
        }

        .label-text {
            alignment-baseline: middle;
            font-size: 12px;
            font-family: arial, helvetica, "sans-serif";
            fill: #393939;
        }

        .label-line {
            stroke-width: 1;
            stroke: #393939;
        }

        .label-circle {
            fill: #393939;
        }


    </style>
@endsection
@section('content')
    <section class="container">
        <section class="hint">
            <div class="ui ordered steps center steps-hint">
                <div class="active step">
                    <div class="content">
                        <div class="title">新增活動進場地點</div>
                        <div class="description">增加進場地點，權限控管！</div>
                    </div>
                </div>
                <div class="active step">
                    <div class="content">
                        <div class="title">活動</div>
                        <div class="description">發佈活動，推播行銷！</div>
                    </div>
                </div>
                <div class="active step">
                    <div class="content">
                        <div class="title">經營</div>
                        <div class="description">運作社團App！</div>
                    </div>
                </div>
                <div class="completed step">
                    <div class="content">
                        <div class="title">輔助經營</div>
                        <div class="description">踴躍度報表、學生回饋！</div>
                    </div>
                </div>
            </div>
        </section>

        <div class="graph-background">
            <div class="row clearfix">
                <h2 class="ui header pull-left">
                    <div class="content col-md-12">
                        {{$activity->name}}
                        <div class="sub header">票券統計</div>
                    </div>
                </h2>

                <div class="pull-right">
                    <a class="ui orange right ribbon label"><span class="big-text" id="realtime-user">0</span>&nbsp;人正在看</a>

                    <p></p>
                    <a class="ui teal right ribbon label"><span class="big-text" id="page-view">{{$pageView}}</span>&nbsp;次觀看數</a>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-8">
                    <h4>數量比</h4>
                    <svg id="svg1">
                        <g id="canvas">
                            <g id="art"></g>
                            <g id="labels"></g>
                        </g>
                    </svg>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <h4>銷售統計</h4>

                    <div id="svg-container">

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('page_js')
    <script src="https://cdn.socket.io/socket.io-1.3.7.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var socket = io("{{url('/')}}" + ":3000");

            var aid = parseInt("{{$activity->id}}");
            var counter = 0;
            var pageView = parseInt("{{$pageView}}");
            socket.emit('lookup.realtime.user', "{{$activity->id}}");

            socket.on('realtime.user.report', function (report) {
                var report = JSON.parse(report);
                if (aid == report.activityId) {
                    counter += parseInt(report.count);
                    $('#realtime-user').text(counter);
                }
            });

            socket.on('user.add.report', function (report) {
                console.log("aid : " + aid);
                var report = JSON.parse(report);
                if (aid == parseInt(report.activityId)) {
                    counter++;
                    pageView++;
                    $('#realtime-user').text(counter);
                    $('#page-view').text(pageView);
                }
            });

            socket.on('user.leave.report', function (report) {
                var report = JSON.parse(report);
                if (aid == parseInt(report.activityId)) {
                    counter--;
                    $('#realtime-user').text(counter);
                }
            });

            var jobPieTotal = $.Deferred();
            (function (d3, $) {
                var amountRequest = $.ajax({
                    url: '{{url('/group/dashboard/activity/' . $activity->id . '/amount')}}'
                });
                amountRequest.done(renderPie);

                function renderPie(data) {
                    var color = d3.scale.category20();
                    var labelNames = [];
                    var total = 0;
                    for (var key in data) {
                        var item = data[key];
                        labelNames.push(item.label);
                        total += item.instances;
                    }

                    var colorMap = labelNames.map(function (label, index) {
                        return {
                            label: label,
                            color: color(index)
                        };
                    });
                    jobPieTotal.resolve(colorMap);
                    var svg = d3.select("#svg1");
                    var canvas = d3.select("#canvas");
                    var art = d3.select("#art");
                    var labels = d3.select("#labels");

                    var jhw_pie = d3.layout.pie();
                    jhw_pie.value(function (d, i) {
                        return d.instances;
                    });

                    var cDim = {
                        height: 400,
                        width: 600,
                        innerRadius: 50,
                        outerRadius: 150,
                        labelRadius: 175
                    };

                    svg.attr({
                        height: cDim.height,
                        width: cDim.width
                    });

                    canvas.attr("transform", "translate(" + (cDim.width / 2) + "," + (cDim.height / 2) + ")");

                    var pied_data = jhw_pie(data);
                    var pied_arc = d3.svg.arc()
                            .innerRadius(70)
                            .outerRadius(150);
                    var enteringArcs = art.selectAll(".wedge").data(pied_data).enter();

                    enteringArcs.append("path")
                            .attr("class", "wedge")
                            .attr("d", pied_arc)
                            .style("fill", function (d, i) {
                                for (var i = 0; i < colorMap.length; i++) {
                                    var colorInfo = colorMap[i];
                                    if (colorInfo.label == d.data.label) {
                                        return colorInfo.color;
                                    }
                                }
                            });

                    var enteringLabels = labels.selectAll(".label").data(pied_data).enter();
                    var labelGroups = enteringLabels.append("g").attr("class", "label");
                    labelGroups.append("circle").attr({
                        x: 0,
                        y: 0,
                        r: 2,
                        fill: "#000",
                        transform: function (d, i) {
                            var centroid = pied_arc.centroid(d);
                            return "translate(" + pied_arc.centroid(d) + ")";
                        },
                        'class': "label-circle"
                    });

                    var textLines = labelGroups.append("line").attr({
                        x1: function (d, i) {
                            return pied_arc.centroid(d)[0];
                        },
                        y1: function (d, i) {
                            return pied_arc.centroid(d)[1];
                        },
                        x2: function (d, i) {
                            var centroid = pied_arc.centroid(d);
                            var midAngle = Math.atan2(centroid[1], centroid[0]);
                            var x = Math.cos(midAngle) * cDim.labelRadius;
                            return x;
                        },
                        y2: function (d, i) {
                            var centroid = pied_arc.centroid(d);
                            var midAngle = Math.atan2(centroid[1], centroid[0]);
                            var y = Math.sin(midAngle) * cDim.labelRadius;
                            return y;
                        },
                        'class': "label-line"
                    });

                    var textLabels = labelGroups.append("text").attr({
                        x: function (d, i) {
                            var centroid = pied_arc.centroid(d);
                            var midAngle = Math.atan2(centroid[1], centroid[0]);
                            var x = Math.cos(midAngle) * cDim.labelRadius;
                            var sign = (x > 0) ? 1 : -1;
                            var labelX = x + (5 * sign);
                            return labelX;
                        },
                        y: function (d, i) {
                            var centroid = pied_arc.centroid(d);
                            var midAngle = Math.atan2(centroid[1], centroid[0]);
                            var y = Math.sin(midAngle) * cDim.labelRadius;
                            return y;
                        },
                        'text-anchor': function (d, i) {
                            var centroid = pied_arc.centroid(d);
                            var midAngle = Math.atan2(centroid[1], centroid[0]);
                            var x = Math.cos(midAngle) * cDim.labelRadius;
                            return (x > 0) ? "start" : "end";
                        },
                        'class': 'label-text'
                    }).text(function (d) {
                        var number = (d.data.instances / total) * 100;
                        var percent = parseInt(number);
                        return d.data.label + " " + percent + "%"
                    });

                    var alpha = 0.5;
                    var spacing = 12;

                    function relax() {
                        var again = false;
                        textLabels.each(function (d, i) {
                            a = this;
                            da = d3.select(a);
                            y1 = da.attr("y");
                            textLabels.each(function (d, j) {
                                b = this;
                                if (a == b) return;
                                db = d3.select(b);
                                if (da.attr("text-anchor") != db.attr("text-anchor")) return;
                                y2 = db.attr("y");
                                deltaY = y1 - y2;

                                if (Math.abs(deltaY) > spacing) return;

                                again = true;
                                sign = deltaY > 0 ? 1 : -1;
                                adjust = sign * alpha;
                                da.attr("y", +y1 + adjust);
                                db.attr("y", +y2 - adjust);
                            });
                        });
                        if (again) {
                            var labelElements = textLabels[0];
                            textLines.attr("y2", function (d, i) {
                                var labelForLine = d3.select(labelElements[i]);
                                return labelForLine.attr("y");
                            });
                            setTimeout(relax, 20)
                        }
                    }

                    relax();
                };
            })(d3, $);

            $.when(jobPieTotal).done(function (colorMap) {
                (function (d3, $, colorMap) {
                    var margin = {top: 50, right: 20, bottom: 30, left: 150},
                            width = 960 - margin.left - margin.right,
                            height = 500 - margin.top - margin.bottom;
                    var x0 = d3.scale.ordinal().rangeRoundBands([0, width], .1);
                    var x1 = d3.scale.ordinal();
                    var y = d3.scale.linear().range([height, 0]);

                    var xAxis = d3.svg.axis()
                            .scale(x0)
                            .orient("bottom");

                    var yAxis = d3.svg.axis()
                            .scale(y)
                            .orient("left")
                            .tickFormat(d3.format(".2s"));

                    var svg = d3.select("#svg-container").append("svg")
                            .attr("width", "100%")
                            .attr("height", height + margin.top + margin.bottom)
                            .append("g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    var salesStatisticsRequest = $.ajax({
                        url: '{{url('/group/dashboard/activity/' . $activity->id .'/statistics')}}'
                    });
                    salesStatisticsRequest.done(renderStatistics);
                    function renderStatistics(data) {
                        var ticketNames = [];
                        for (var i = 0; i < data.length; i++) {
                            var daySoldInfo = data[i];
                            for (var key in daySoldInfo) {
                                if (key != 'State') {
                                    ticketNames.push(key);
                                }
                            }
                        }

                        function unique(array) {
                            return $.grep(array, function (el, index) {
                                return index == $.inArray(el, array);
                            });
                        }

                        ticketNames = unique(ticketNames);

                        for (var i = 0; i < data.length; i++) {
                            var daySoldInfo = data[i];
                            ticketNames.forEach(function (ticketName) {
                                if (!daySoldInfo[ticketName]) {
                                    daySoldInfo[ticketName] = 0;
                                }
                            });
                        }

                        data.forEach(function (d) {
                            d.ages = ticketNames.map(function (name) {
                                return {name: name, value: +d[name]};
                            });
                        });

                        x0.domain(data.map(function (d) {
                            return d.State;
                        }));

                        x1.domain(ticketNames).rangeRoundBands([0, x0.rangeBand()]);
                        var max = d3.max(data, function (d) {
                            return d3.max(d.ages, function (d) {
                                return d.value;
                            });
                        });
                        y.domain([0, max + (max / 5)]);

                        svg.append("g")
                                .attr("class", "x axis")
                                .attr("transform", "translate(0," + height + ")")
                                .call(xAxis);

                        svg.append("g")
                                .attr("class", "y axis")
                                .call(yAxis)
                                .append("text")
                                .attr("transform", "rotate(-90)")
                                .attr("y", 6)
                                .attr("dy", ".71em")
                                .style("text-anchor", "end")
                                .text("銷售數量");


                        var state = svg.selectAll(".state")
                                .data(data)
                                .enter().append("g")
                                .attr("class", "g")
                                .attr("transform", function (d) {
                                    return "translate(" + x0(d.State) + ",0)";
                                });

                        state.selectAll("rect")
                                .data(function (d) {
                                    return d.ages;
                                })
                                .enter().append("rect")
                                .attr("width", x1.rangeBand())
                                .attr("x", function (d) {
                                    return x1(d.name);
                                })
                                .attr("y", function (d) {
                                    return y(d.value);
                                })
                                .attr("height", function (d) {
                                    return height - y(d.value);
                                })
                                .style("fill", function (d) {
                                    for (var i = 0; i < colorMap.length; i++) {
                                        var colorInfo = colorMap[i];
                                        if (colorInfo.label == d.name) {
                                            return colorInfo.color;
                                        }
                                    }
                                });

                        var legend = svg.selectAll(".legend")
                                .data(ticketNames.slice().reverse())
                                .enter().append("g")
                                .attr("class", "legend")
                                .attr("transform", function (d, i) {
                                    return "translate(0," + i * 20 + ")";
                                });

                        legend.append("rect")
                                .attr("x", width - 18)
                                .attr("width", 18)
                                .attr("height", 18)
                                .style("fill", function (d) {
                                    for (var i = 0; i < colorMap.length; i++) {
                                        var colorInfo = colorMap[i];
                                        if (colorInfo.label == d) {
                                            return colorInfo.color;
                                        }
                                    }
                                });

                        legend.append("text")
                                .attr("x", width - 24)
                                .attr("y", 9)
                                .attr("dy", ".35em")
                                .style("text-anchor", "end")
                                .text(function (d) {
                                    return d;
                                });
                    }
                })(d3, $, colorMap);
            });
        });
    </script>
@endsection