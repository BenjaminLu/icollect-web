@extends('app')

@section('content')
    <section class="container">
        @include('partial.message')
        <section class="hint">
            <div class="ui ordered steps center steps-hint">
                <div class="active step">
                    <div class="content">
                        <div class="title">新增活動進場地點</div>
                        <div class="description">增加進場地點，權限控管！</div>
                    </div>
                </div>
                <div class="completed step">
                    <div class="content">
                        <div class="title">活動</div>
                        <div class="description">發佈活動，推播行銷！</div>
                    </div>
                </div>
                <div class="active step">
                    <div class="content">
                        <div class="title">經營</div>
                        <div class="description">運作社團App！</div>
                    </div>
                </div>
                <div class="active step">
                    <div class="content">
                        <div class="title">輔助經營</div>
                        <div class="description">踴躍度報表、學生回饋！</div>
                    </div>
                </div>
            </div>
        </section>
        <div id="main-control" class="clearfix">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <a id="add-activity-btn" class="ui left floated small primary labeled icon button center"
                       href="{{url('/activity/create')}}">
                        <i class="plus icon"></i> 增加活動
                    </a>
                </div>
                <div class="col-md-6 col-sm-4">

                </div>
                <div class="col-md-3  col-sm-5 col-xs-6">
                    <input id="activity-search" type="text" class="form-control pull-right" placeholder="搜尋...">
                </div>
            </div>
        </div>
        <div>
            <table id="activity-table" class="ui striped table-hover table">
                <thead class="full-width">
                <tr>
                    <th class="two wide">活動編號</th>
                    <th class="eleven wide">活動名稱</th>
                    <th class="three wide">控制</th>
                </tr>
                </thead>
                <tbody>
                @foreach($activities as $activity)
                    <tr>
                        <td><span>{{$activity->id}}</span></td>
                        <td>
                            <span>
                                <a href="{{url('/activity/'. $activity->id)}}">{{$activity->name}}</a>
                            </span>
                        </td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li>
                                        <a href="{{url('/activity/' . $activity->id . '/edit')}}">編輯</a>
                                    </li>
                                    <li>
                                        <a href="{{url('/group/dashboard/activity/' . $activity->id)}}">票券統計</a>
                                    </li>
                                    <li>
                                        <a href="{{url('/group/activity/' . $activity->id . '/ticket/report')}}">參與報表</a>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a target="_blank" href="{{url('/group/download/activity/' . $activity->id . '/ticket/report')}}">下載參與報表</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endsection

@section('page_js')
    <script src="https://cdn.socket.io/socket.io-1.3.7.js"></script>
    <script>
        $(document).ready(function () {
            kendo.culture("zh-CHT");

            $('.alert').delay(3000).slideUp(300);
        });
    </script>
@endsection