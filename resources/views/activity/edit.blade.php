@extends('app')
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')

    <section class="container">
        @include('partial.message')
        <section>
            <div class="ui ordered steps center steps-hint">
                <div class="active step">
                    <div class="content">
                        <div class="title">新增活動進場地點</div>
                        <div class="description">增加進場地點，權限控管！</div>
                    </div>
                </div>
                <div class="completed step">
                    <div class="content">
                        <div class="title">活動</div>
                        <div class="description">發佈活動，推播行銷！</div>
                    </div>
                </div>
                <div class="active step">
                    <div class="content">
                        <div class="title">經營</div>
                        <div class="description">運作社團App！</div>
                    </div>
                </div>
                <div class="active step">
                    <div class="content">
                        <div class="title">輔助經營</div>
                        <div class="description">踴躍度報表、學生回饋！</div>
                    </div>
                </div>
            </div>
        </section>
        <div id="edit-activity" class="content activity-form">
            <div class="row">
                <form id="update-activity-form" method="post"
                      class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="put"/>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <labal class="col-md-3 control-label">活動名稱</labal>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="name" value="{{$activity->name}}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <labal class="col-md-3 control-label">活動圖片</labal>
                        <div class="col-md-7">
                            <div class="form-group">
                                <div class="col-md-7 col-xs-7">
                                    <input class="form-control" placeholder="Choose File"
                                           id="filename" name="image_url" value="{{$activity->image}}"/>
                                </div>
                                <div class="col-md-2 col-xs-2 fileUpload ui button positive">
                                    <i class="fa fa-folder-open"></i>
                                    <input type="file" class="upload" name="image" id="image"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <labal class="col-md-3 control-label">活動內容</labal>
                        <div class="col-md-7">
                            <textarea cols="80" rows="10" id="editor1" name="body"
                                      class="form-control">{{$activity->body}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <labal class="col-md-3 control-label">活動地點</labal>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="location" value="{{$activity->location}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <labal class="col-md-3 control-label">取票截止日期</labal>
                        <div class="col-md-2">
                            <input id="gain_start" class="datepicker" name="gain_start"
                                   value="{{$activity->gain_start}}"/>
                        </div>
                        <div class="col-md-1" align="center">
                            <labal class="control-label">至</labal>
                        </div>
                        <div class="col-md-1">
                            <input id="gain_end" class="datepicker" name="gain_end" value="{{$activity->gain_end}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <labal class="col-md-3 control-label">使用時間</labal>
                        <div class="col-md-2">
                            <input id="ex_start" class="datepicker" name="ex_start" value="{{$activity->ex_start}}"/>
                        </div>
                        <div class="col-md-1" align="center">
                            <labal class="control-label">至</labal>
                        </div>
                        <div class="col-md-1">
                            <input id="ex_end" class="datepicker" name="ex_end" value="{{$activity->ex_end}}"/>
                        </div>
                    </div>
                    <section class="ex-section">
                        <div class="row">
                            <div class="col-md-3 control-label form-title">
                                <div class="header">票卷設定
                                    <i class="fa fa-plus-circle plus-btn ex-plus-btn"></i>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        @if(count($exRules) > 0)
                            <?php $j = 0; ?>
                            @foreach($exRules as $exRule)
                                <div class="form-group ex-row row">
                                    <input type="hidden" name="ex_rules[<?php echo $j;?>][id]"
                                           class="form-control ex-row-id" value="{{$exRule->id}}"/>

                                    <div class="col-md-3 text-right">
                                        <i class="fa fa-minus-circle minus-btn ex-minus-btn"
                                           data-index="<?php echo $j;?>"></i>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="gift-photo">
                                            @if($exRule->image)
                                                <img src="{{url($exRule->image)}}" class="img-responsive"/>
                                            @else
                                                No Image
                                            @endif
                                        </div>
                                    </div>
                                    <input type="hidden" name="ex_rules[<?php echo $j;?>][image]"
                                           class="form-control ex-row-image-url" value="{{$exRule->image}}"/>

                                    <div class="col-md-4">
                                        <input type="text" name="ex_rules[<?php echo $j;?>][name]"
                                               class="form-control ex-row-name" placeholder="票卷名稱"
                                               data-index="<?php echo $j;?>" value="{{$exRule->name}}"/>
                                    </div>
                                    <div class="col-md-1">
                                        <input type="number" name="ex_rules[<?php echo $j;?>][quantity]"
                                               class="form-control col-md-1 ex-row-quantity" placeholder="數量" min="0"
                                               data-index="<?php echo $j;?>" value="{{$exRule->quantity}}"/>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" name="ex_rules[<?php echo $j;?>][body]"
                                               class="form-control ex-row-body" placeholder="票卷描述"
                                               data-index="<?php echo $j;?>" value="{{$exRule->body}}"/>
                                    </div>
                                </div>
                                <?php $j++; ?>
                            @endforeach
                        @endif
                    </section>
                    <div class="actions">
                        <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-4">
                                <button id="preview-activity" data-name="preview" class="ui button positive" type="submit">
                                    預覽
                                </button>
                            </div>
                            <div class="col-md-4">
                                <button id="submit-activity" data-name="submit" class="ui button positive" type="submit">
                                    儲存
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('page_js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            kendo.culture("zh-CHT");

            $(".datepicker").kendoDatePicker({
                format: "yyyy-MM-dd",
                animation: {
                    close: {
                        effects: "fadeOut zoom:out",
                        duration: 300
                    },
                    open: {
                        effects: "fadeIn zoom:in",
                        duration: 300
                    }
                }
            });

            $('.alert').delay(3000).slideUp(300);
            var fileNameInput = $('#filename');
            $('#image').on('change', function () {
                var filename = $(this).val().split('\\').pop();
                fileNameInput.val(filename);
            });

            var gainNumber = 0;
            var exNumber = 0;
            var gainSection = $('.gain-section');
            var exSection = $('.ex-section');
            var gainRowControlStack = new RowControlStack();
            var exRowControlStack = new RowControlStack();

            var makeGainRow = function () {
                var minusBtn = $('<i>').addClass('fa fa-minus-circle minus-btn gain-minus-btn').attr('data-index', gainNumber);
                var inputBody = $('<input>').attr({
                    'type': 'text',
                    'name': 'gain_rules[' + gainNumber + '][body]',
                    'placeholder': '集點規則',
                    'data-index': gainNumber
                }).addClass('form-control gain-row-body');

                var inputValue = $('<input>').attr({
                    'type': 'number',
                    'name': 'gain_rules[' + gainNumber + '][value]',
                    'placeholder': '點數',
                    'min': 0,
                    'data-index': gainNumber
                }).addClass('form-control col-md-1 gain-row-value');

                var inputID = $('<input>').attr({
                    'type': 'hidden',
                    'name': 'gain_rules[' + gainNumber + '][id]'
                }).addClass('form-control gain-row-id');
                var minusBtnDiv = $('<div>').addClass('col-md-3 text-right');
                var bodyDiv = $('<div>').addClass('col-md-4');
                var thresholdDiv = $('<div>').addClass('col-md-1');

                $(minusBtnDiv).append(minusBtn);
                $(bodyDiv).append(inputBody);
                $(thresholdDiv).append(inputValue);

                var gainRow = $('<div>').addClass('form-group gain-row');
                return $(gainRow).append(inputID, minusBtnDiv, bodyDiv, thresholdDiv);
            };

            var initGainSection = function () {
                var size = parseInt({{count($gainRules)}});
                for (var i = 0; i < size; i++) {
                    gainRowControlStack.push(gainNumber);
                    gainNumber = gainRowControlStack.getNextIndex();
                }
            };

            initGainSection();

            //make new gain row
            $('.gain-plus-btn').on('click', function () {
                $(gainSection).append(makeGainRow());
                gainRowControlStack.push(gainNumber);
                gainNumber = gainRowControlStack.getNextIndex();
            });

            $(document).on('click', '.gain-minus-btn', function () {
                var thisIndex = $(this).attr('data-index');
                var thisIndexNumber = parseInt(thisIndex);
                $('.gain-row').find('[data-index=' + thisIndex + ']').parents('.gain-row').remove();
                gainRowControlStack.remove(thisIndexNumber);
                gainRowControlStack.updateIndexesOfGainSection(gainSection);
                gainNumber = gainRowControlStack.getNextIndex();
            });

            //Ex Section Control
            var makeExchangeRow = function () {
                var minusBtn = $('<i>').addClass('fa fa-minus-circle minus-btn ex-minus-btn').attr('data-index', exNumber);
                var giftPhoto = $('<div>').addClass('gift-photo').text('No Image');

                var giftImageUrl = $('<input>').attr({
                    'type': 'hidden',
                    'name': 'ex_rules[' + exNumber + '][image]'
                }).addClass('form-control ex-row-image-url');

                var inputName = $('<input>').attr({
                    'type': 'text',
                    'name': 'ex_rules[' + exNumber + '][name]',
                    'placeholder': '票卷名稱',
                    'data-index': exNumber
                }).addClass('form-control ex-row-name');

                var inputQuantity = $('<input>').attr({
                    'type':'number',
                    'name':'ex_rules[' + exNumber +'][quantity]',
                    'placeholder':'數量',
                    'min':0,
                    'data-index':exNumber
                }).addClass('form-control col-md-1 ex-row-threshold');

                var inputBody = $('<input>').attr({
                    'type': 'text',
                    'name': 'ex_rules[' + exNumber + '][body]',
                    'placeholder': '票卷描述',
                    'data-index': exNumber
                }).addClass('form-control ex-row-body');

                var inputID = $('<input>').attr({
                    'type': 'hidden',
                    'name': 'ex_rules[' + exNumber + '][id]'
                }).addClass('form-control ex-row-id');

                var minusBtnDiv = $('<div>').addClass('col-md-3 text-right');
                var giftPhotoDiv = $('<div>').addClass('col-md-2');
                var nameDiv = $('<div>').addClass('col-md-4');
                var quantityDiv = $('<div>').addClass('col-md-1');
                var bodyDiv = $('<div>').addClass('col-md-5');

                $(minusBtnDiv).append(minusBtn);
                $(giftPhotoDiv).append(giftPhoto);
                $(nameDiv).append(inputName);
                $(quantityDiv).append(inputQuantity);
                $(bodyDiv).append(inputBody);

                var exRow = $('<div>').addClass('form-group ex-row row');
                return $(exRow).append(inputID, minusBtnDiv, giftPhotoDiv, giftImageUrl, nameDiv, quantityDiv, bodyDiv);
            };

            var initExSection = function () {
                var size = parseInt({{count($exRules)}});
                for (var i = 0; i < size; i++) {
                    exRowControlStack.push(exNumber);
                    exNumber = exRowControlStack.getNextIndex();
                }
            };

            initExSection();

            $('.ex-plus-btn').on('click', function () {
                $(exSection).append(makeExchangeRow());
                exRowControlStack.push(exNumber);
                exNumber = exRowControlStack.getNextIndex();
            });

            $(document).on('click', '.ex-minus-btn', function () {
                var thisIndex = $(this).attr('data-index');
                var thisIndexNumber = parseInt(thisIndex);
                console.log(thisIndexNumber);
                $('.ex-row').find('[data-index=' + thisIndex + ']').parents('.ex-row').remove();
                exRowControlStack.remove(thisIndexNumber);
                exRowControlStack.updateIndexesOfExSection(exSection);
                3
                exNumber = exRowControlStack.getNextIndex();
            });

            $(document).on('dragover', '.gift-photo', function (e) {
                e.preventDefault();
                e.stopPropagation();

                $(this).css({
                    'border': '2px dashed #2A2'
                });
            });

            $(document).on('dragenter', '.gift-photo', function (e) {
                e.preventDefault();
                e.stopPropagation();
            });

            $(document).on('dragleave', '.gift-photo', function (e) {
                e.preventDefault();
                e.stopPropagation();

                $(this).css({
                    'border': '2px dashed #ccc'
                });
            });

            $(document).on('drop', '.gift-photo', function (e) {
                var _this = this;
                if (e.originalEvent.dataTransfer) {
                    if (e.originalEvent.dataTransfer.files.length) {
                        e.preventDefault();
                        e.stopPropagation();
                        var dt = e.dataTransfer || (e.originalEvent && e.originalEvent.dataTransfer);
                        var file = e.target.files || (dt && dt.files);

                        var formData = new FormData();
                        var csrfToken = document.getElementsByName('_token');
                        formData.append('gift-image', file[0]);

                        $.ajax({
                            url: '{{url('/photo/upload')}}',
                            type: "post",
                            data: formData,
                            processData: false,
                            contentType: false,
                            success: function (data) {
                                console.log(data);
                                d = new Date();
                                var url = data['url'];
                                $(_this).parent().siblings('.ex-row-image-url').val(url);
                                var imageURL = '{{url('/')}}' + '/' + url + "?" + d.getTime();
                                var image = $('<img>').attr({
                                    'src': imageURL
                                }).addClass('img-responsive');

                                $(_this).empty().append(image);

                                //FadeIn effect
                                $(image).hide();
                                var div = $('<div>').text("Loading...");
                                $(_this).append(div);
                                if (image.complete) {
                                    $(image).fadeIn(500);
                                    $(div).text("");
                                } else {
                                    $(image).load(function () {
                                        $(image).fadeIn(500);
                                        $(div).text("");
                                    });
                                }
                                $(_this).css({
                                    'border': '2px solid #ccc'
                                });
                            },
                            fail: function (data) {
                                alert(data);
                                $(_this).css({
                                    'border': '2px dashed #ccc'
                                });
                            },
                            error: function (xhr, ajaxoptions, thrownerror) {
                                $(_this).css({
                                    'border': '2px dashed #ccc'
                                });
                                alert(xhr.status);
                                alert(thrownerror);
                            }
                        });
                    }
                }
            });
            var form = $('#update-activity-form');
            var submitBtn = $('#submit-activity');
            var previewBtn = $('#preview-activity');
            var btn = null;

            form.on('submit', function() {
                if(btn == 'submit') {
                    form.attr("action", '{{url('/activity/' . $activity->id)}}');
                } else if(btn == 'preview') {
                    form.attr("action", "{{url('/activity/preview')}}");
                }
                return true;
            });

            submitBtn.on('click', function() {
                btn = 'submit';
            });

            previewBtn.on('click', function() {
                btn = 'preview';
            });
        });
    </script>
@endsection