@extends('app')
@section('title', ' - '. "所有活動")
@section('content')
    <section class="container clearfix">
        <div class="row">
            <section class="col-md-8 col-md-offset-2">
                <form method="get" class="col-md-12" id="search-form">
                    <div class="ui fluid category search">
                        <div class="ui action input">
                            <input class="prompt" id="search-keyword" placeholder="活動、社團、地址、票卷..." type="text">
                            <select class="ui compact selection dropdown" id="category-select">
                                <option value="activity">活動</option>
                                <option value="company">社團</option>
                                <option value="address">地址</option>
                                <option value="gift">票卷</option>
                            </select>
                            <button type="submit" class="ui button" id="search-submit-btn">搜尋</button>
                        </div>
                        <div class="results"></div>
                    </div>
                </form>
            </section>
        </div>
        <section class="row col-md-12">
            @if(count($activities) > 0)
                <section class="col-md-12">
                    <h2 class="section-title">
                        <span>精彩活動</span>
                    </h2>
                </section>
                <?php $count = count($activities); ?>
                <?php $last = ($count >= 6) ? 6 : $count?>
                @for($i = 0 ; $i < $last ; $i++)
                    <?php $activity = $activities[$i]; ?>
                    <div class="col-md-4 search-item-box">
                        <div class="search-item-container col-md-12">
                            <div class="col-md-12 search-item-image-container">
                                <a href="{{url('/activity/' . $activity->id)}}">
                                    <img src="{{url('/resources/loading.gif')}}"
                                         data-original="{{url($activity->image)}}"
                                         class="img-responsive  search-item-image lazy"/>
                                </a>
                            </div>
                            <div class="inner-container">
                                <div class="col-md-12 search-item-body-container">
                                    <div class="col-md-12  search-item-title">
                                        <a href="{{url('/activity/' . $activity->id)}}">{{$activity->name}}</a>
                                    </div>
                                    <div class="col-md-12 search-item-body">
                                        <?php $paragraphs = explode(PHP_EOL, $activity->body); ?>
                                        @foreach($paragraphs as $paragraph)
                                            {{{ $paragraph }}}
                                            <br/>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="search-item-time-container col-md-12">
                            <i class="fa fa-calendar"></i>
                            <?php $gainStartDate = \Carbon\Carbon::parse($activity->gain_start);?>
                            <?php $gainEndDate = \Carbon\Carbon::parse($activity->gain_end);?>
                            <span class="green-text">{{$gainStartDate->month}}月{{$gainStartDate->day}}日</span>
                            至
                            <span class="green-text">{{$gainEndDate->month}}月{{$gainEndDate->day}}日</span>
                            取票
                        </div>
                    </div>
                @endfor
                @if($count >= 7)
                    <section class="col-md-12">
                        <h2 class="section-title">
                            <span>熱情參與</span>
                        </h2>
                    </section>
                    <?php $last = ($count >= 8) ? 8 : $count?>
                    @for($i = 6 ; $i < $last ; $i++)
                        <?php $activity = $activities[$i]; ?>
                        <div class="col-md-6">
                            <div class="search-item-container col-md-12">
                                <div class="col-md-12 search-item-image-container">
                                    <a href="{{url('/activity/' . $activity->id)}}">
                                        <img src="{{url('/resources/loading.gif')}}"
                                             data-original="{{url($activity->image)}}"
                                             class="img-responsive  search-item-image lazy"/>
                                    </a>
                                </div>
                                <div class="inner-container">
                                    <div class="col-md-12 search-item-body-container">
                                        <div class="col-md-12  search-item-title">
                                            <a href="{{url('/activity/' . $activity->id)}}">{{$activity->name}}</a>
                                        </div>
                                        <div class="col-md-12 search-item-body">
                                            <?php $paragraphs = explode(PHP_EOL, $activity->body); ?>
                                            @foreach($paragraphs as $paragraph)
                                                {{{ $paragraph }}}
                                                <br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="search-item-time-container col-md-12">
                                <i class="fa fa-calendar"></i>
                                <?php $gainStartDate = \Carbon\Carbon::parse($activity->gain_start);?>
                                <?php $gainEndDate = \Carbon\Carbon::parse($activity->gain_end);?>
                                <span class="green-text">{{$gainStartDate->month}}月{{$gainStartDate->day}}日</span>
                                至
                                <span class="green-text">{{$gainEndDate->month}}月{{$gainEndDate->day}}日</span>
                                取票
                            </div>
                        </div>
                    @endfor
                @endif
                @if($count >= 9)
                    <section class="col-md-12">
                        <h2 class="section-title">
                            <span>豐富生活</span>
                        </h2>
                    </section>
                    <?php $last = ($count >= 11) ? 11 : $count?>
                    @for($i = 8 ; $i < $last ; $i++)
                        <?php $activity = $activities[$i]; ?>
                        <div class="col-md-4">
                            <div class="search-item-container col-md-12">
                                <div class="col-md-12 search-item-image-container">
                                    <a href="{{url('/activity/' . $activity->id)}}">
                                        <img src="{{url('/resources/loading.gif')}}"
                                             data-original="{{url($activity->image)}}"
                                             class="img-responsive  search-item-image lazy"/>
                                    </a>
                                </div>
                                <div class="inner-container">
                                    <div class="col-md-12 search-item-body-container">
                                        <div class="col-md-12  search-item-title">
                                            <a href="{{url('/activity/' . $activity->id)}}">{{$activity->name}}</a>
                                        </div>
                                        <div class="col-md-12 search-item-body">
                                            <?php $paragraphs = explode(PHP_EOL, $activity->body); ?>
                                            @foreach($paragraphs as $paragraph)
                                                {{{ $paragraph }}}
                                                <br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="search-item-time-container col-md-12">
                                <i class="fa fa-calendar"></i>
                                <?php $gainStartDate = \Carbon\Carbon::parse($activity->gain_start);?>
                                <?php $gainEndDate = \Carbon\Carbon::parse($activity->gain_end);?>
                                <span class="green-text">{{$gainStartDate->month}}月{{$gainStartDate->day}}日</span>
                                至
                                <span class="green-text">{{$gainEndDate->month}}月{{$gainEndDate->day}}日</span>
                                取票
                            </div>
                        </div>
                    @endfor
                @endif

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <nav>
                            <ul class="pager">
                                @if($page <= 1)
                                    <li class="previous disabled"><a href="#"><span aria-hidden="true">&larr;</span> 往前</a>
                                    </li>
                                @else
                                    <li class="previous"><a href="{{url('/activity/all/page/' . ($page - 1))}}"><span
                                                    aria-hidden="true">&larr;</span> 往前</a></li>
                                @endif

                                @if($page == $lastPage)
                                    <li class="next disabled"><a href="#">往後 <span aria-hidden="true">&rarr;</span></a>
                                    </li>
                                @else
                                    <li class="next"><a href="{{url('/activity/all/page/' . ($page + 1))}}">往後 <span
                                                    aria-hidden="true">&rarr;</span></a></li>
                                @endif
                            </ul>
                        </nav>
                    </div>
                </div>
            @else
                <div class="row col-md-6 col-md-offset-3 text-center">
                    <span>抱歉，沒有結果</span>
                </div>
            @endif
        </section>
    </section>
    @include('partial.footer')
@endsection

@section('page_js')
    <script>
        $(document).ready(function () {
            kendo.culture("zh-CHT");
            $('.item').on('click', function () {
                $('.item').removeClass('active');
                $(this).addClass('active');
            });

            $('.ui.dropdown').dropdown();
            $('.ui.search').search({
                apiSettings: {
                    url: '{{url('/api/search/{query}')}}'
                },
                type: 'category',
                searchDelay: '500',
                cache: true,
                onSelect: function (value, response) {
                    if (value != undefined) {
                        var category;
                        if (response.category1 != undefined) {
                            var length = response.category1.results.length;
                            for (var i = 0; i < length; i++) {
                                var item = response.category1.results[i];
                                if (item.title == value.title) {
                                    category = response.category1.name;
                                }
                            }
                        }

                        if (response.category2 != undefined) {
                            var length = response.category2.results.length;
                            for (var i = 0; i < length; i++) {
                                var item = response.category2.results[i];
                                if (item.title == value.title) {
                                    category = response.category2.name;
                                }
                            }
                        }

                        if (response.category3 != undefined) {
                            var length = response.category3.results.length;
                            for (var i = 0; i < length; i++) {
                                var item = response.category3.results[i];
                                if (item.title == value.title) {
                                    category = response.category3.name;
                                }
                            }
                        }

                        if (response.category4 != undefined) {
                            var length = response.category4.results.length;
                            for (var i = 0; i < length; i++) {
                                var item = response.category4.results[i];
                                if (item.title == value.title) {
                                    category = response.category4.name;
                                }
                            }
                        }
                        console.log(category);
                        $('#category-select').dropdown('set selected', category);
                        return "default";
                    } else {
                        return false;
                    }
                }
            });

            $("img.lazy").lazyload({
                effect: "fadeIn"
            });

            $('.search-item-container').hover(function () {
                $(this).find('.inner-container').stop().animate({
                    'margin-top': '90px'
                }, 400, 'easeOutCubic')
            }, function () {
                $(this).find('.inner-container').stop().animate({
                    'margin-top': '164px'
                }, 400, 'easeOutCubic')
            });

            $('#search-submit-btn').on('click', function () {
                var keyword = $('#search-keyword').val();
                var type = $('#category-select :selected').text();
                console.log(keyword);
                console.log(type);
                var baseURL = '{{url('/activity/search/')}}' + '/' + keyword + '/type/' + type + '/page/1';
                $('#search-form').attr('action', baseURL)[0].submit();
            });
        });
    </script>
@endsection