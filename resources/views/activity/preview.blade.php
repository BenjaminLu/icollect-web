@extends('app')
@section('title', ' - '. $activity->name)
@section('content')
    <section class="container show-activity">
        <div class="frame">
            <section class="title col-md-12">
                <span> {{$activity->name}}</span>
            </section>
            <div class="body-frame">
                <section class="banner">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 col-xs-12">
                            @if($activity->image)
                                <img class="img-responsive banner-image" src="{{url($activity->image)}}"
                                     alt="{{$activity->name}}"/>
                            @endif
                        </div>
                    </div>
                </section>
                <hr>
                <section>
                    <div class="row">
                        <div class="col-md-12 header">
                            <i class="fa fa-sitemap"></i>
                            <span class="white-text">主辦單位</span>
                        </div>
                        <div class="col-md-12 content">
                            {{$group->name}} - {{$group->address}}
                        </div>
                    </div>
                </section>
                <section>
                    <div class="row">
                        <div class="col-md-12 header">
                            <i class="fa fa-star"></i>
                            <span class="white-text">活動內容</span>
                        </div>
                        <div class="col-md-12 content">
                            <?php use Carbon\Carbon;$paragraphs = explode(PHP_EOL, $activity->body); ?>
                            @foreach($paragraphs as $paragraph)
                                {{ $paragraph }}
                                <br/>
                            @endforeach
                        </div>
                    </div>
                </section>
                <section>
                    <?php
                        Carbon::setLocale('zh-TW');
                        $gainStart = Carbon::parse($activity->gain_start);
                        $exStart = Carbon::parse($activity->ex_start);
                        $gainEnd = Carbon::parse($activity->gain_end)->addDay();
                        $exEnd = Carbon::parse($activity->ex_end)->addDay();
                        $now = Carbon::now();
                        $gainDiffForHuman = $gainEnd->diffForHumans($now);
                        $exDiffForHuman = $exEnd->diffForHumans($now);
                        $isTicketOpening = false;
                        $isActivityOpening = false;
                        $beforeTakeTicket = false;
                        $beforeTakeTicketDiff = null;
                        $beforeActivity = false;
                        $beforeActivityDiff = null;
                        if($now->lt($gainStart)) {
                            $beforeTakeTicket = true;
                            $beforeTakeTicketDiff = $gainStart->diffForHumans($now);
                        }

                        if($now->lt($exStart)) {
                            $beforeActivity = true;
                            $beforeActivityDiff = $exStart->diffForHumans($now);
                        }

                        if ($now->gte($gainStart) && $now->lt($gainEnd)) {
                            $isTicketOpening = true;
                        }

                        if ($now->gte($exStart) && $now->lt($exEnd)) {
                            $isActivityOpening = true;
                        }
                    ?>
                    <div class="row">
                        <div class="col-md-12 header">
                            <i class="fa fa-calendar"></i>
                            <span class="white-text">活動時間</span>
                        </div>
                        <div class="col-md-12 content">
                            <div class="collect">
                                <span>取票日期 : </span>
                                <span class="date">{{$activity->gain_start}}</span> 至
                                <span class="date">{{$activity->gain_end}} &nbsp;</span>
                                @if($beforeTakeTicket)
                                    <span class="badge">取票於{{$beforeTakeTicketDiff}}開始</span>
                                @elseif($isTicketOpening)
                                    <span class="badge">取票至{{$gainDiffForHuman}}</span>
                                @else
                                    <span class="badge">取票已結束</span>
                                @endif
                            </div>
                            <div class="exchange">
                                <span>使用日期 : </span>
                                <span class="date">{{$activity->ex_start}}</span> 至
                                <span class="date">{{$activity->ex_end}} &nbsp;</span>
                                @if($beforeActivity)
                                    <span class="badge">活動於{{$beforeActivityDiff}}開始</span>
                                @elseif($isActivityOpening)
                                    <span class="badge">活動至{{$exDiffForHuman}}</span>
                                @else
                                    <span class="badge">活動已結束，感謝各位參與</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="row">
                        <div class="col-md-12 header">
                            <i class="fa fa-gift"></i>
                            <span class="white-text">票卷列表</span>
                        </div>
                        <div class="col-md-12 content">
                            <div class="row">
                                @foreach($exRules as $exRule)
                                    <div class="col-sm-4 col-md-3">
                                        <div class="thumbnail">
                                            @if($exRule['image'])
                                                <img src="{{url('/' . $exRule['image'])}}">
                                            @else
                                                <div class="show-gift-photo">No Image</div>
                                            @endif
                                            <div class="caption text-center">
                                                <h3>{{$exRule['name']}}</h3>
                                                <p>{{$exRule['body']}}</p>
                                                <p>剩 <span class="big-text red-text">{{$exRule['quantity']}}</span> 張</p>
                                                @if(Auth::check() and $isTicketOpening)
                                                    <div class="row">
                                                        <div class="col-xs-12 number-picker">
                                                            <div class="input-group number-spinner">
                                                                <span class="input-group-btn data-dwn">
                                                                    <button class="btn btn-default btn-info"
                                                                            data-dir="dwn"><span
                                                                                class="glyphicon glyphicon-minus"></span>
                                                                    </button>
                                                                </span>
                                                                <input type="text"
                                                                       class="form-control text-center ticket-number-picker"
                                                                       value="0" min="0" max="10"/>
                                                                <span class="input-group-btn data-up">
                                                                    <button class="btn btn-default btn-info"
                                                                            data-dir="up"><span
                                                                                class="glyphicon glyphicon-plus"></span>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>
                @if(Auth::check() and $isTicketOpening)
                    <section class="buy-statistics-section">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4 text-center">
                                <button id="buy-btn" type="button" class="btn btn-info center">取票</button>
                                <button id="send-ticket-gift-btn" type="button" class="btn btn-info center">贈票</button>
                            </div>
                        </div>
                    </section>
                @endif
                <section>
                    <div class="row">
                        <div class="col-md-12 header">
                            <i class="fa fa-map-marker"></i>
                            <span><a target="_blank" class="content" href="http://maps.google.com/?daddr={{$activity->location}}">{{$activity->location}}</a></span>
                        </div>
                        <div class="col-md-12">
                             <div id="map_canvas" style="height:500px; width:100%"></div> 
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div id="disqus_thread"></div>
    </section>

    <section>
        <div id="send-ticket-gift-modal" class="ui modal">
            <div class="header">
                <i class="edit icon"></i>
                贈送票卷
            </div>
            <div class="content">
                <div class="row">
                    <form id="send-ticket-gift-form" class="form-horizontal" onsubmit="return false;">
                        <div class="row">
                            <label class="col-xs-2" for="email">寄給</label>

                            <div class="col-xs-10 ui search">
                                <input type="text" class="form-control prompt" id="email" placeholder="收件人E-mail">

                                <div class="results"></div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-xs-12" for="gift-message">送禮訊息</label>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <textarea class="form-control" id="gift-message" rows="10"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="actions">
                <div id="submit-ticket-gifts" class="ui button positive">
                    確認
                </div>
                <div class="ui button">
                    取消
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page_js')
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script>
        var map;
        var activty_location = '{{$activity->location}}';
        function initialize() {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': activty_location}, function(results, status) {
                var myLatLng = results[0].geometry.location;
                var mapOptions = {
                    zoom: 17,
                    center: myLatLng,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                if (status == google.maps.GeocoderStatus.OK) {
                    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection