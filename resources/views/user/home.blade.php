@extends('app')

@section('content')

    <section class="container">
        @include('partial.message')

        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#unused" class="ticket-type-tab" aria-controls="unused"
                                                          role="tab" data-toggle="tab">未使用</a></li>
                <li role="presentation"><a href="#used" class="ticket-type-tab" aria-controls="used" role="tab"
                                           data-toggle="tab">已使用</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="unused">
                    <section class="tickets_container">
                        @if($unusedTicketsActivities)
                            @foreach($unusedTicketsActivities as $unusedTicketsActivity)
                                <h1>{{$unusedTicketsActivity->name}}</h1>
                                <div class="row">
                                    @if($unusedTickets)
                                        @foreach($unusedTickets as $unusedTicket)
                                            @if($unusedTicket->activity_id == $unusedTicketsActivity->id)
                                                <div class="col-sm-4 col-md-3">
                                                    <div class="thumbnail  target-ticket" id="{{$unusedTicket->id}}">
                                                        @if($unusedTicket->image)
                                                            <img src="{{url($unusedTicket->image)}}">
                                                        @else
                                                            <div class="home-ticket-photo">No Image</div>
                                                        @endif

                                                        <div class="caption text-center">
                                                            <h3>票號 : {{$unusedTicket->id}} {{$unusedTicket->name}}</h3>

                                                            <p>{{$unusedTicket->body}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    </section>
                </div>
                <div role="tabpanel" class="tab-pane" id="used">
                    <section class="tickets_container">
                        @if($usedTicketsActivities)
                            @foreach($usedTicketsActivities as $usedTicketsActivity)
                                <h1>{{$usedTicketsActivity->name}}</h1>
                                <div class="row">
                                    @if($usedTickets)
                                        @foreach($usedTickets as $usedTicket)
                                            @if($usedTicket->activity_id == $usedTicketsActivity->id)
                                                <div class="col-sm-4 col-md-3">
                                                    <div class="thumbnail  target-ticket" id="{{$usedTicket->id}}">
                                                        @if($usedTicket->image)
                                                            <img src="{{url($usedTicket->image)}}">
                                                        @else
                                                            <div class="home-ticket-photo">No Image</div>
                                                        @endif

                                                        <div class="caption text-center">
                                                            <h3>票號 : {{$usedTicket->id}} {{$usedTicket->name}}</h3>

                                                            <p>{{$usedTicket->body}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    </section>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page_js')
    <script>
        $(document).ready(function () {
            kendo.culture("zh-CHT");
            $("#datetimepicker").kendoDateTimePicker({
                value: new Date(),
                format: "yyyy/MM/dd HH:mm"
            });
            $('.ui.checkbox').checkbox();

            $('.alert').delay(3000).slideUp(300);

            $('a.ticket-type-tab').click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            })
        });
    </script>
@endsection