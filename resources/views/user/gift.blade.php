@extends('app')
@section('title', ' - 收到的票卷禮物')
@section('content')
    <section>
        <div class="container">
            <div class="row grid">
                @foreach($ticketGifts as $ticketGift)
                    <?php $activity = $ticketGift->activity;?>
                    <?php $tickets = $ticketGift->tickets;?>
                    <?php $sender = $ticketGift->sender;?>
                    <?php $message = $ticketGift->message;?>
                    <div class="col-xs-12 col-sm-4 col-md-3 grid-item">
                        <div class="ui card center-card">
                            <div class="image">
                                <img src="{{url($activity->image)}}">
                            </div>
                            <div class="content">
                                <a class="header one-line" href="{{url('/activity/' . $activity->id)}}"
                                   target="_blank">{{$activity->name}}</a>
                                <hr/>
                                <div class="meta">
                                    <span class="date text-green">{{$activity->ex_start}}</span>
                                    <span>至</span>
                                    <span class="date text-green">{{$activity->ex_end}}</span>
                                    <span>間使用</span>
                                </div>
                                <div class="description">
                                    <h6><span class="text-orange">{{$sender->name}}</span> 想對您說 :</h6>

                                    <div>
                                        {{$message}}
                                    </div>
                                </div>
                            </div>
                            <div class="extra content">
                                <span class="help-block">{{'總共' . count($tickets) . '張票卷'}}</span>
                                <ul class="list-group limit-height-list">
                                    @foreach($tickets as $ticket)
                                        <?php $isUsed = ($ticket->status)?>
                                        <li class="list-group-item">
                                            <a href="{{url('/my/ticket' . '#' . $ticket->id)}}" target="_blank">
                                                <i class="fa fa-ticket"></i>
                                                {{$ticket->name}}
                                                @if($isUsed)
                                                    <a class="ui red empty circular label pull-right"></a>
                                                @else
                                                    <a class="ui green empty circular label pull-right"></a>
                                                @endif
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
@section('page_js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.min.js"></script>
    <script>
        $('.grid').masonry({
            // options
            itemSelector: '.grid-item',
            percentPosition: true
        });
    </script>
@endsection