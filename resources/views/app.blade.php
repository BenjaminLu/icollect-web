<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @yield('meta')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>中興活動 @yield('title')</title>
    <link rel="stylesheet" href="{{elixir('css/all.css')}}"/>
    <link rel="stylesheet" href="{{elixir('css/components.css')}}">
    @yield('css')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/', null, false)}}">中興活動</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="{{url('/activity/all', null, false)}}">所有活動</a></li>
                @if( session('group_check') )
                    <li><a href="{{ url('/group/home', null, false) }}">驗票裝置管理</a></li>
                    <li><a href="{{ url('/activity', null ,false) }}">活動管理</a></li>
                @endif
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if( Auth::check() )
                    <li>
                        <a href="{{ url('/my/ticket', null ,false) }}">我的票卷
                            @if(session()->get('new_tickets_number'))
                                <span class="badge badge-info">{{session()->get('new_tickets_number')}}</span>
                            @endif
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/ticket/gift', null, false) }}">票卷禮物
                        </a>
                    </li>
                @endif

                @if (Auth::guest() and !session('group_check'))
                    <li><a href="{{ url('/login', null , false) }}">登入</a></li>
                    <li><a href="{{ url('/register', null , false) }}">註冊</a></li>
                @endif

                @if( session('group_check') )
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">{{ session('group')->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/group/logout') }}">從社團登出</a></li>
                        </ul>
                    </li>
                @endif

                @if (Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/auth/logout') }}">從學生登出</a></li>
                        </ul>
                    </li>
                @endif

                <?php $auth = auth()->guard('admin');?>
                @if ($auth->check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">{{ $auth->user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/admin/logout') }}">從管理員登出</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

@yield('content')

@yield('footer')

<script src="{{elixir('js/all.js')}}"></script>
<script src="{{elixir('js/app.blade.js')}}"></script>
@yield('page_js')
</body>
</html>
