@extends('app')

@section('content')
    <div id="login-section" class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 alert text-center" role="alert">
                <h3><strong>歡迎!</strong> 請選擇您的角色並且登入</h3>
            </div>
        </div>
        <div class="row">
            <!-- User login -->
            <div id="user-login-section" class="col-md-4 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading"><h4>學生</h4></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>登入輕鬆查看活動與取票</h2>
                            </div>
                        </div>
                        <hr/>
                        <div class="form-group row">
                            <div class="col-md-4 col-md-offset-4 text-center">
                                <a href="{{secure_url('/auth/login')}}" class="btn btn-info">
                                    我是學生
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--  Group login  -->
            <div id="group-login-section" class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4>社團</h4></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>登入經營社團活動</h2>
                            </div>
                        </div>
                        <hr/>
                        <div class="form-group row">
                            <div class="col-md-4 col-md-offset-4 text-center">
                                <a href="{{secure_url('/group/login')}}" class="btn btn-primary">
                                    我是社團
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
