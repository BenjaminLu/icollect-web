@extends('app')

@section('content')
    <div id="register-section" class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 alert text-center" role="alert">
                <h3><strong>歡迎!</strong> 請選擇您的角色並且註冊</h3>
            </div>
        </div>
        <div class="row">
            <!-- user register -->
            <div class="col-md-4 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading"><h4>學生</h4></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>免費功能</h2>
                            </div>
                        </div>
                        <hr/>
                        <div id="user-register-features">
                            <ul class="list-group borderless">
                                <li class="list-group-item default"><i class="glyphicon glyphicon-ok"></i>
                                    免費使用學生 App
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    查詢活動訊息、LBS查詢附近社團活動
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    方便快速的取票卷
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    票卷證據掌握在學生手中
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    電子票劵也能贈禮，遠距離的好朋友也能連絡感情
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    使用票卷超簡單，核銷當下學生不需要網路，適用wifi族群
                                </li>
                            </ul>
                        </div>
                        <hr/>

                        <div class="form-group row">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{secure_url('/auth/register')}}" class="btn btn-info">
                                    我是學生
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--  Group register  -->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4>社團</h4></div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>免費功能</h2>
                            </div>
                        </div>
                        <hr/>
                        <div id="group-register-features">
                            <ul class="list-group borderless">
                                <li class="list-group-item default"><i class="glyphicon glyphicon-ok"></i>
                                    免費使用社團 App
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    免費刊登活動、發放票卷
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    快速推播活動訊息至特定客群手機，達成精準行銷
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    核銷票卷超簡單，教育訓練成本低
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    活動場地無需佈設網路即可核銷票卷，超低導入門檻
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    票卷防偽機制，防止惡意複製，偽造假票卷
                                </li>
                            </ul>
                        </div>
                        <hr/>

                        <div class="form-group row">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{secure_url('/group/register')}}" class="btn btn-primary">
                                    我是社團
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partial.footer')
@endsection
