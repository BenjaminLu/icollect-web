@extends('app')

@section('content')

    <section class="container">
        @include('partial.message')
        <section class="hint">
            <div class="ui ordered steps center steps-hint">
                <div class="completed step">
                    <div class="content">
                        <div class="title">新增活動驗票裝置</div>
                        <div class="description">增加驗票裝置，權限控管！</div>
                    </div>
                </div>
                <div class="active step">
                    <div class="content">
                        <div class="title">活動</div>
                        <div class="description">發佈活動，推播行銷！</div>
                    </div>
                </div>
                <div class="active step">
                    <div class="content">
                        <div class="title">經營</div>
                        <div class="description">運作社團App！</div>
                    </div>
                </div>
                <div class="active step">
                    <div class="content">
                        <div class="title">輔助經營</div>
                        <div class="description">踴躍度報表、學生回饋！</div>
                    </div>
                </div>
            </div>
        </section>
        <div id="main-control" class="clearfix">
            <div class="row">
                <div class="col-md-3  col-sm-3 col-xs-4">
                    <div id="add-store-btn" class="ui left floated small primary labeled icon button center">
                        <i class="plus icon"></i> 增加驗票裝置帳號
                    </div>
                </div>
                <div id="show-password-container" class="col-md-6 col-sm-4 col-xs-3">
                    <div class="ui toggle checkbox pull-left">
                        <input id="show-password-btn" type="checkbox" class="password-control">
                        <label>顯示密碼</label>
                    </div>
                </div>
                <div class="col-md-3  col-sm-5 col-xs-5">
                    <input id="store-search" type="text" class="form-control pull-right" placeholder="搜尋...">
                </div>
            </div>
        </div>
        <!--<div id="example">
            <div id="to-do">
                <input id="datetimepicker">
            </div>
        </div>-->
        <div>
            <table id="store-table" class="ui compact definition table">
                <thead class="full-width">
                <tr>
                    <th>帳號</th>
                    <th>驗票裝置名稱</th>
                    <th>地址</th>
                    <th>電話</th>
                    <th>App登入密碼</th>
                    <th>提供網路</th>
                    <th>控制</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div id="add-store-modal" class="ui modal">
            <div class="header">
                <i class="edit icon"></i>
                新增驗票裝置
            </div>
            <div class="content">
                <div class="row">
                    <form id="add-store-form" action="{{url('/store')}}" method="post" class="form-horizontal">
                        <div class="form-group">
                            <labal class="col-md-4 control-label">驗票裝置名稱</labal>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <labal class="col-md-4 control-label">地址</labal>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="address"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <labal class="col-md-4 control-label">電話</labal>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="tel"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <labal class="col-md-4 control-label">社團App登入密碼</labal>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <labal class="col-md-4 control-label">有無網路</labal>
                            <div class="ui toggle checkbox col-md-3 semantic-checkbox-container">
                                <input id="network-switch" type="checkbox" name="has_network">
                                <label>有網路</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="actions">
                <div id="submit-store" class="ui button positive">
                    新增
                </div>
                <div class="ui button">
                    取消
                </div>
            </div>
        </div>

        <div id="update-store-modal" class="ui modal">
            <div class="header">
                <i class="edit icon"></i>
                編輯驗票裝置
                <span id="update-store-id"></span>
            </div>
            <div class="content">
                <div class="row">
                    <form id="update-store-form" action="{{url('/store')}}" method="post" class="form-horizontal">
                        <div class="form-group">
                            <labal class="col-md-4 control-label">驗票裝置名稱</labal>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <labal class="col-md-4 control-label">地址</labal>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="address"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <labal class="col-md-4 control-label">電話</labal>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="tel"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <labal class="col-md-4 control-label">社團App登入密碼</labal>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <labal class="col-md-4 control-label">有無網路</labal>
                            <div class="ui toggle checkbox col-md-3 semantic-checkbox-container">
                                <input id="network-switch" type="checkbox" name="has_network">
                                <label>有網路</label>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="actions">
                <div id="submit-update-store" class="ui button positive">
                    儲存
                </div>
                <div class="ui button">
                    取消
                </div>
            </div>
        </div>

        <div id="error-modal" class="ui modal">
            <div class="header">
                <i class="warning  icon"></i>
                錯誤
            </div>
            <div class="content">
                提交失敗，請確認活動地點資料符合規定，或通知iCollect客服中心。
            </div>
            <div class="actions">
                <div id="submit-store" class="ui button">
                    確認
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page_js')
    <script>
        $(document).ready(function () {
            kendo.culture("zh-CHT");

            var storeTableControl = new StoreTableControl({
                table: $('#store-table'),
                passwordControl: $('#show-password-btn'),
                searchControl: $('#store-search'),
                initURL: "{{url('/stores/belongsto/group/' . session('group')->id)}}",
                updateModal: $('#update-store-modal'),
                updateURL: "{{url('/store/')}}",
                csrfToken: "{{ csrf_token() }}"
            });

            $('#submit-store').on('click', function () {
                var addStoreForm = $('#add-store-modal');
                var data = {
                    '_token': "{{csrf_token()}}",
                    'name': addStoreForm.find('input[name=name]').val(),
                    'address': addStoreForm.find('input[name=address]').val(),
                    'tel': addStoreForm.find('input[name=tel]').val(),
                    'password': addStoreForm.find('input[name=password]').val(),
                    'has_network': addStoreForm.find('input[name=has_network]').is(':checked')
                };
                $.ajax({
                    url: '{{url('/store')}}',
                    data: data,
                    type: 'POST',
                    dataType: 'json',

                    success: function (store) {
                        storeTableControl.appendNewStoreToTable(store);
                    },

                    error: function (xhr, ajaxOptions, thrownError) {
                        $('#error-modal').modal('show');
                    }
                });
            });

            $('#add-store-btn').on('click', function () {
                $('#add-store-modal').modal('setting', 'transition', 'drop').modal('show');
            });

            $('#add-activity-btn').on('click', function () {
                window.location.href = '/group/activity';
            });

            $("#datetimepicker").kendoDateTimePicker({
                value: new Date(),
                format: "yyyy/MM/dd HH:mm"
            });

            $('.alert').delay(3000).slideUp(300);
        });
    </script>
@endsection