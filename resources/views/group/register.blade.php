@extends('app')

@section('content')
    <div id="register-section" class="container-fluid">
        <div class="row">
            <!--  Group register  -->
            <div id="group-register-section" class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h4>社團</h4></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>免費功能</h2>
                            </div>
                        </div>
                        <hr/>
                        <div id="group-register-features">
                            <ul class="list-group borderless">
                                <li class="list-group-item default"><i class="glyphicon glyphicon-ok"></i>
                                    免費使用社團 App
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    免費刊登活動、發放票卷
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    快速推播活動訊息至特定客群手機，達成精準行銷
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    核銷票卷超簡單，教育訓練成本低
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    活動地點無需佈設網路即可核銷票卷，超低導入門檻
                                </li>
                                <li class="list-group-item important"><i class="glyphicon glyphicon-ok"></i>
                                    票卷防偽機制，防止惡意複製，偽造假票卷
                                </li>
                            </ul>
                        </div>
                        <hr/>

                        <form id="group-register-form" class="form-horizontal" role="form" method="POST"
                              action="{{ url('/group/register') }}">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">社團名稱</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">E-Mail</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">密碼</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">確認密碼</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">類型</label>

                                <div class="col-md-6">
                                    <select class="ui search dropdown" name="type">
                                        <option value="">請選擇社團類型</option>
                                        <option>學術性</option>
                                        <option>學藝性</option>
                                        <option>服務性</option>
                                        <option>聯誼性</option>
                                        <option>體育性</option>
                                        <option>康樂性</option>
                                        <option>自治性</option>
                                        <option>綜合性</option>
                                        <option>其他</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">電話</label>

                                <div class="col-md-6">
                                    <input type="tel" class="form-control" name="tel" value="{{ old('tel') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">地址</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="address" value="{{old('address')}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-1 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        註冊
                                    </button>
                                </div>

                                <div class="col-md-1">
                                    <a href="{{url('/register')}}" class="btn btn-primary rollback-btn">
                                        返回
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partial.footer')
@endsection

@section('page_js')
    <script>
        $(document).ready(function () {
            $('.dropdown').dropdown({
                transition: 'drop'
            });
        });
    </script>
@endsection
