<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    <title>中興活動 - 最方便的社團活動電子票卷系統</title>
    <link rel="stylesheet" href="{{elixir('css/all.css')}}"/>
    <link rel="stylesheet" href="{{elixir('css/welcome.css')}}"/>
</head>
<body>
<div class="head-segment">
    <video class="opening" loop autoplay poster="{{url('/resources/poster.png')}}">
        <source src="{{url('/resources/eventsLoop.mp4')}}" type="video/mp4">
    </video>
    <!--[if lt IE 9]>
    <script>
        document.createElement('video');
    </script>
    <![endif]-->
    <div>
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" id="nav-hamburger" class="navbar-toggle collapsed"
                                    data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="{{url('/activity/all')}}">所有活動</a></li>
                                @if(session('group_check'))
                                    <li>
                                        <a href="{{url('/group/home')}}">驗票裝置管理</a>
                                    </li>
                                    <li>
                                        <a href="{{url('/activity')}}">活動管理</a>
                                    </li>
                                @endif

                                @if(Auth::check())
                                    <li>
                                        <a href="{{url('/my/ticket')}}">我的票卷</a>
                                    </li>
                                @endif

                                @if(Auth::guest() and !session('group_check'))
                                    <li>
                                        <a href="{{url('/login')}}">登入</a>
                                    </li>
                                    <li>
                                        <a href="{{url('/register')}}">註冊</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="introduction wow bounceInDown invisible">
                            <div class="col-sm-12 col-lg-12 title-msg">
                                <span>中興活動</span>
                                <span class="small-text">BETA</span>
                                <p><b>享受精彩生活</b></p>
                            </div>
                            <div class="col-sm-12">
                                <a href="{{url('/activity/all')}}">
                                    <div class="ui inverted button" id="newbtn">最新活動！</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row feature-segment">
        <div class="col-sm-12 col-lg-6">
            <div class="feature wow fadeInUp invisible" data-wow-offset="100">
                <i class="fa fa-qrcode feature-img"></i>

                <p>最方便快速的購票、核銷，超簡單的消費流程</p>
            </div>
        </div>
        <div class="col-sm-12 col-lg-6">
            <div class="feature wow fadeInUp invisible" data-wow-offset="100">
                <i class="fa fa-facebook-square feature-img"></i>

                <p>快速的社群傳播，透過Facebook，twitter，立即擴散活動資訊！</p>
            </div>
        </div>
    </div>
    <div class="ui divider"></div>
    <div class="row feature-segment">
        <div class="col-sm-12 col-lg-6">
            <div class="feature wow fadeInUp invisible">
                <i class="fa fa-bolt feature-img"></i>

                <p>核銷票卷，獨立店不需要網路，學生使用快速，社團導入門檻超低！</p>
            </div>
        </div>
        <div class="col-sm-12 col-lg-6">
            <div class="feature wow fadeInUp invisible">
                <i class="fa fa-bar-chart feature-img"></i>

                <p>社團不需自行建置系統，完整的圖表統計，追蹤活動參與狀況，輔助社團經營</p>
            </div>
        </div>
    </div>
</div>
<div class="middle-segment">
    <div class="opening spin-container">
        <img src="{{url('/resources/middle_bg.png')}}" class="spin-object"/>
    </div>
    <div class="container">
        <div class="row">
            <div class="introduction">
                <div class="col-md-4">
                    <img src="{{url('/resources/cards.png')}}" class="img-responsive center middle-img">
                </div>
                <div class="col-md-8 middle-msg wow flipInX invisible">
                    <span> 中興活動 </span>

                    <p><b>參與活動，如此簡單</b></p>
                </div>
                <div class="col-sm-12 wow fadeInUp invisible">
                    <a href="{{url('/register')}}">
                        <div class="ui inverted button" id="register-btn">
                            現在就加入！
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row feature-segment">
        <div class="col-sm-12 col-lg-6">
            <div class="feature wow fadeInLeft invisible">
                <i class="fa fa-users feature-img"></i>

                <p>完整贈票功能，邀請朋友一同參與！</p>
            </div>
        </div>
        <div class="col-sm-12 col-lg-6">
            <div class="feature wow fadeInRight invisible">
                <i class="fa fa-map-marker feature-img"></i>

                <p>快速查詢活動、票卷、社團相關活動位置，立即線下享受服務！</p>
            </div>
        </div>
    </div>
    <div class="ui divider"></div>
    <div class="row feature-segment">
        <div class="col-sm-12 col-lg-6">
            <div class="feature wow fadeInLeft invisible">
                <i class="fa fa-lock feature-img"></i>

                <p>完整票卷加密與防偽機制，你的票卷你掌握，學生掌握證據，社團免擔心偽造票卷</p>
            </div>
        </div>
        <div class="col-sm-12 col-lg-6">
            <div class="feature wow fadeInRight invisible">
                <i class="fa fa-mobile feature-img"></i>

                <p>精準的推播行銷，根據學生活動參與類型，直接推播多媒體訊息到手機，不錯過適合你的活動！</p>
            </div>
        </div>
    </div>
</div>
@include('partial.footer')
<script src="{{elixir('js/all.js')}}"></script>
<script>
    wow = new WOW({offset:220});
    wow.init();
</script>
</body>
</html>
